WARNING_FLAGS = -Wall -std=c99

LIB = -lm -lSDL2main -lSDL2 -lSDL2_ttf -lSDL2_image -lSDL2_net -lSDL2_gfx -lSDL2_mixer -lGL -lGLU -lglut `pkg-config --cflags --libs opencv`

.PHONY: clean all

all: Cubulus



Cubulus: MoteurJeu.o MoteurGraphique.o MoteurReseau.o MoteurVideo.o main.o makefile

	gcc -o Cubulus MoteurJeu.o MoteurGraphique.o MoteurReseau.o MoteurVideo.o main.o ${LIB}

MoteurJeu.o: MoteurJeu.h MoteurJeu.c makefile

	gcc ${WARNING_FLAGS} -o MoteurJeu.o -c MoteurJeu.c

MoteurGraphique.o: MoteurGraphique.h MoteurGraphique.c makefile

	gcc ${WARNING_FLAGS} -o MoteurGraphique.o -c MoteurGraphique.c

MoteurReseau.o: MoteurReseau.h MoteurReseau.c makefile

	gcc ${WARNING_FLAGS} -o MoteurReseau.o -c MoteurReseau.c

MoteurVideo.o: MoteurVideo.h MoteurVideo.c makefile

	gcc ${WARNING_FLAGS} -o MoteurVideo.o -c MoteurVideo.c

main.o: main.c makefile

	gcc ${WARNING_FLAGS} -o main.o -c main.c

clean:

	rm -f *.o *~ Cubulus



