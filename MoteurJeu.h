#ifndef MOTEURJEU_H
#define MOTEURJEU_H

/**
 * \file MoteurJeu.h
 * \brief Contient les prototypes des différrentes fonctions du module du moteur jeu,
 *		  ce module utilise des librairies standard pour faire fonctionner le cubulus.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>

/**
 * \def TAILLE_CUBE
 * \brief Définit la taille du cube en nombre de billes
 */
#define TAILLE_CUBE 3


/**
 * \enum Face
 * \brief Liste les differentes faces du cube
*/
enum Face
{
    F_DESSUS, F_DESSOUS, F_ARRIERE, F_FACE, F_GAUCHE, F_DROITE, F_ARRIERE_INVERSE, F_MILIEU
};

/**
 * \enum Direction
 * \brief Liste les differentes direction d'insertion de billes
*/
enum Direction
{
    DROITE, GAUCHE, HAUT, BAS
};

/**
 * \enum Couleur
 * \brief Liste les differentes couleur des billes, VOID pour l'absence de billes
*/
enum Couleur
{
    BLANC, NOIR, ROUGE, VOID
};


/**
 * \struct Sphere
 * \brief Structure définissant une sphere, avec la couleur de cette dernière
*/
struct Sphere
{
    enum Couleur couleur;
};
typedef struct Sphere Sphere;

/**
 * \struct Joueur
 * \brief Structure définissant un joueur,
 * 		  avec son nom, sa couleur de bille, et sa caracteristique (IA ou humain),
 * 		  et son niveau si c'est un IA
*/
struct Joueur
{
    enum Couleur couleur;
    char* nom;
    bool joueur;
    bool joueurReseau;
    bool aTonTour;
    int niveauIA;
};
typedef struct Joueur Joueur;

/**
 * \struct Cubulus
 * \brief Structure définissant un cubulus avec ses caractéristiques
*/
struct Cubulus
{
    bool troisJoueur;
    struct Joueur joueurs[3];
    Sphere tableau[TAILLE_CUBE][TAILLE_CUBE][TAILLE_CUBE];
    Sphere historique[TAILLE_CUBE][TAILLE_CUBE][TAILLE_CUBE];
    int billeRestante[3];
    bool gagne;
    enum Couleur couleurDebut;
};
typedef struct Cubulus Cubulus;


/**
 * \fn VideCube(Cubulus *cubulus)
 * \brief Initialise le cube de vide
 * \param cubulus, Structure Cubulus
 */
void VideCube(Cubulus* cubulus);

/**
 * \fn ParametrerCube(Cubulus* cubulus, Joueur j1, Joueur j2, Joueur j3)
 * \brief Paramètre le cube en fonction des options de joueurs choisies
 * \param cubulus, Structure CubulusContext
 * \param j1 structure Joueur correspondant au joueur 1
 * \param j2 structure Joueur correspondant au joueur 2
 * \param j3 structure Joueur correspondant au joueur 3
 * \return true si les paramètres sont correcte, false sinon
 */
bool ParametrerCube(Cubulus* cubulus, Joueur j1, Joueur j2, Joueur j3);

/**
 * \fn void FaceCube(Cubulus *cubulus, Sphere tab[TAILLE_CUBE][TAILLE_CUBE], enum Face face, bool ecrire)
 * \brief Remplit la face demandé dans un tableau 2D
 * \param cubulus Structure Cubulus
 * \param tab Tableau 2D de type Sphere
 * \param face Enumeration enum Face
 * \param ecrire Lecture/Ecriture : true si il faut écrire, false si il faut lire
 */
void FaceCube(Cubulus *cubulus, Sphere tab[2][TAILLE_CUBE][TAILLE_CUBE], enum Face face, bool ecrire);

/**
 * \fn VerifieFace(Sphere tableau[TAILLE_CUBE][TAILLE_CUBE])
 * \brief Vérifie s'il y a un gain sur une face
 * \param tableau tableau de structure Sphere
 * \return La couleur qui à gagné (VOID si personne)
 */
enum Couleur VerifieFace(Sphere tableau[TAILLE_CUBE][TAILLE_CUBE]);

/**
 * \fn VerifieCube(Cubulus* cubulus)
 * \brief Vérifie s'il y a un gain sur tout le cube
 * \param cubulus Structure Cubulus
 * \return La couleur qui à gagné (VOID si personne)
 */
enum Couleur VerifieCube(Cubulus* cubulus);

/**
 * \fn AjouterBoule(Cubulus *cubulus, int x, int y, enum Couleur couleurJ)
 * \brief Ajoute si possible une boule à l'endroit indiqué sur la grille de jeu
 * \param cubulus Structure Cubulus
 * \param x Abscisse
 * \param y Ordonnée
 * \param couleurJ Couleur de la boule à ajouter
 * \return true si la boule est ajouté, false sinon
 */
bool AjouterBoule(Cubulus *cubulus, int x, int y, enum Couleur couleurJ);

/**
 * \fn AjouteBouleCote(Sphere face[TAILLE_CUBE][TAILLE_CUBE], enum Direction entree, enum Couleur newCouleur, int rang, Cubulus *cubulus)
 * \brief Ajoute une boule si possible à une face
 * \param face Face sur laquel il faut ajouté une sphere
 * \param entree Direction dans laquel il faut ajouté une boule
 * \param newCouleur Couleur de la sphere à ajouté
 * \param rang Ranger de la face où ajouté une boule
 * \param cubulus Structure Cubulus
 * \return 0 si la boule n'a pas pu être joué
            1 si une boule à était ajouté
            2 si une boule à état déplacé
 */
int AjouteBouleCote(Sphere face[TAILLE_CUBE][TAILLE_CUBE], enum Direction entree, enum Couleur newCouleur, int rang, Cubulus *cubulus);

/**
 * \fn EstRemplis(Cubulus *cubulus)
 * \brief Vérifie si le cube est rempli
 * \param cubulus Structure Cubulus
 * \return true si le cube est plein, false sinon
 */
bool EstRemplis(Cubulus *cubulus);

/**
 * \fn Copie(Cubulus *cubulus1, Cubulus *cubulus2)
 * \brief Copie deux structures Cubulus
 * \param cubulus1 Structure Cubulus source
 * \param cubulus2 Structure Cubulus
 */
void Copie(Cubulus *cubulus1, Cubulus *cubulus2);


/**
 * \fn CopieTableau3D(Sphere ***tab1, Sphere ***tab2)
 * \brief Copie deux tableaux 3D
 * \param tab1 Structure Cubulus source
 * \param tab2 Structure Cubulus
 */
void CopieTableau3D(Sphere tab1[TAILLE_CUBE][TAILLE_CUBE][TAILLE_CUBE], Sphere tab2[TAILLE_CUBE][TAILLE_CUBE][TAILLE_CUBE]);


/**
 * \fn CompareTableau3D(Sphere ***tab1, Sphere ***tab2)
 * \brief Compare deux tableaux 3D
 * \param tab1 Structure Cubulus
 * \param tab2 Structure Cubulus
 * \return true si les deux tableau sont identiques
 */
bool CompareTableau3D(Sphere tab1[TAILLE_CUBE][TAILLE_CUBE][TAILLE_CUBE], Sphere tab2[TAILLE_CUBE][TAILLE_CUBE][TAILLE_CUBE]);

/**
 * \fn Jouer(Cubulus *cubulus, int x, int y, bool simulationIA)
 * \brief Joue le prochain tour
 * \param cubulus Structure Cubulus
 * \param x Abscisse sur la grille de jeu
 * \param y Ordonnee sur la grille de jeu
 * \return true si le coup à été joué
 */
bool Jouer(Cubulus *cubulus, int x, int y);

/**
 * \fn RotationCube(Cubulus* cubulus, enum Direction direction)
 * \brief Modifie le tableau 3D pour faire tourner le cube
 * \param cubulus Structure Cubulus
 * \param direction Sens de rotation
 */
void RotationCube(Cubulus* cubulus, enum Direction direction);

/**
 * \fn EvalueCube(Cubulus *cubulus, enum Couleur couleur,  int* pX, int* pY, int* pFace,  int etage, int etageMax)
 * \brief Fonction récursive qui évalue si le cube est favorable à la couleur ou non
 * \param cubulus Structure Cubulus
 * \param couleur Couleur du joueur / IA
 * \param *pX Pointeur de l'abscisse de la grille de jeu du coup le plus favorable
 * \param *pY Pointeur de l'ordonnée de la grille de jeu du coup le plus favorable
 * \param *pFace Pointeur de la face du cube correspondant au coup le plus favorable
 * \param etage Etage actuel de l'arbre de récursions (0)
 * \param etageMax Etage à laquel la récursion doit s'arreter
 * \return entier correspondant à l'évaluation du cube (inférieur à 0 si défavorable)
 */
int EvalueCube(Cubulus *cubulus, enum Couleur couleur,  int* pX, int* pY, int* pFace,  int etage, int etageMax);

/**
 * \fn IAJoue(Cubulus *cubulus)
 * \brief Fait jouer l'IA
 * \param cubulus Structure Cubulus
 * \return Face sur laquel l'IA à joué
 */
enum Face IAJoue(Cubulus *cubulus);

/**
 * \fn enum Couleur TireAuSort(void)
 * \brief Tire au sort une couleur entre Rouge et Noir
 * \param cubulus Structure Cubulus
 * \return Couleur qui placera les boules blanches
 */
enum Couleur TireAuSort(Cubulus *cubulus);



#endif // MOTEURJEU_H
