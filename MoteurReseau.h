#ifndef MOTEURRESEAU_H
#define MOTEURRESEAU_H

/**
 * \file MoteurReseau.h
 * \brief Contient les prototypes des différrentes fonctions du module du moteur réseau,
 *		  ce module utilise SDL2_net et SDL2_thread
 */

#include <SDL2/SDL_net.h>
#include <SDL2/SDL_thread.h>
#include <stdbool.h>
#include "MoteurGraphique.h"

#include "MoteurGraphique.h"

/**
 * \def PORT
 * \brief Définit le port à utilisé pour les communications
 */
#define PORT 2000
/**
 * \def TAILLE_TAMPON
 * \brief Taille des paquets à utiliser
 */
#define TAILLE_TAMPON 10
/**
 * \def ADRESSE_VIDE
 * \brief Définition de l'adresse vide
 */
#define ADRESSE_VIDE "                "
/**
 * \def VERSION
 * \brief Définition la version du client
 */
#define VERSION_CLIENT 2


/**
 * \enum Fonctions
 * \brief Liste les differentes fonctions pouvant être envoyé au joueur distant
*/
enum Fonctions
{
    VERSION, JOUER, ROTATION, TIRAGE
};

/**
 * \struct Paquet
 * \brief Liste chainée de paquets
*/
struct Paquet
{
    char tampon[TAILLE_TAMPON];
    struct Paquet *suivant;
};
typedef struct Paquet Paquet;


/**
 * \struct ReseauContext
 * \brief Structure pour l'utilisation du réseau.
*/
struct ReseauContext
{
    TCPsocket serveur;
    TCPsocket ecoute;
    IPaddress ipClient;
    IPaddress ipServeur;

    SDL_Thread *thread;
    bool stopThread;
    bool deconnection;
    bool connecte;

    int erreur;
    char adresse[16];

    Paquet *p;
    bool mouvementLibre;
};
typedef struct ReseauContext ReseauContext;



/**
 * \fn InitialisationReseau(ReseauContext *reseau)
 * \brief Alloue la mémoire nécessaire, et initialise le ReseauContext
 * \param cubulus Structure ReseauContext
 */
bool InitialisationReseau(ReseauContext *reseau);

/**
 * \fn DestroyReseau(ReseauContext *reseau)
 * \brief Libère la mémoire alloué sur le tas utilisé par le ReseauContext et stoppe toutes activitées reseau.
 * \param cubulus Structure ReseauContext
 */
void DestroyReseau(ReseauContext *reseau);


/**
 * \fn Serveur(struct CubulusContext *cubulus)
 * \brief Fonction bloquante, lance un serveur et attend la connection d'un client. Puis ajoute les paquets reçu à la liste chainée.
 * \param cubulus Structure CubulusContext
 */
void Serveur(struct CubulusContext *cubulus);

/**
 * \fn Client(struct CubulusContext *cubulus)
 * \brief Fonction bloquante, lance un client tente de se connecte à un serveur. Puis ajoute les paquets reçu à la liste chainée.
 * \param cubulus Structure CubulusContext
 */
void Client(struct CubulusContext *cubulus);

/**
 * \fn ReceptionPaquet(struct CubulusContext *cubulus)
 * \brief Lis le dernier 1er paquet de la liste chainée et execute l'action que ce dernier commande
 * \param cubulus Structure CubulusContext
 */
void ReceptionPaquet(struct CubulusContext *cubulus);

/**
 * \fn EnvoiPaquet(struct CubulusContext *cubulus, enum Fonctions action, int paraJRL, int paraJL, int paraL)
 * \brief Envoi un paquet au joueur distant
 * \param cubulus Structure CubulusContext
 * \param action Actien à effectuer sur la partie du joueur distant
 * \param paraJRL Paramètre 1
 * \param paraJL Paramètre 2
 * \param paraL Paramètre 3
 * \return false si le paquet n'a pas pu être envoyé
 */
bool EnvoiPaquet(struct CubulusContext *cubulus, enum Fonctions action, int paraJRL, int paraJL, int paraL);

/**
 * \fn ATonTour(struct CubulusContext* Cubulus)
 * \brief Vérifie si le tour est au joueur
 * \param cubulus Structure CubulusContext
 * \return true si le tour est au joueur
 */
bool ATonTour(struct CubulusContext* cubulus);


#endif
