/**
 * \file MoteurReseau.c
 * \brief Contient les différrentes fonctions du module du moteur réseau
 */

#include "MoteurGraphique.h"
#include "MoteurReseau.h"


bool InitialisationReseau(ReseauContext *reseau)
{
    if (SDLNet_Init() < 0)
        return false;

    reseau->stopThread = false;
    reseau->p = NULL;
    reseau->thread = NULL;
    reseau->erreur = 0;
    reseau->deconnection = false;
    reseau->connecte = false;
    reseau->mouvementLibre = false;
    sprintf(reseau->adresse, ADRESSE_VIDE);

    return true;
}

void DestroyReseau(ReseauContext *reseau)
{
    reseau->stopThread = true;
    reseau->deconnection = true;
    reseau->erreur = 0;

    SDLNet_Quit();
}

void Serveur(struct CubulusContext *cubulus)
{
    ReseauContext *reseau = &cubulus->reseau;
    reseau->stopThread = false;
    reseau->deconnection = false;
    reseau->erreur = 0;
	IPaddress *ip;
	Paquet *paquet, *temp;

	if (SDLNet_ResolveHost(&reseau->ipServeur, NULL, PORT) < 0)
	{
		printf("SDLNet_ResolveHost: %s\n", SDLNet_GetError());
		reseau->erreur = -1;
		return;
	}

	if (!(reseau->ecoute = SDLNet_TCP_Open(&reseau->ipServeur)))
	{
		printf("SDLNet_TCP_Open: %s\n", SDLNet_GetError());
		reseau->erreur = -1;
		return;
	}

	while (!reseau->stopThread)
	{
        reseau->serveur = SDLNet_TCP_Accept(reseau->ecoute);
		if (reseau->serveur)
		{
            ip = SDLNet_TCP_GetPeerAddress(reseau->serveur);
			if (ip)
				printf("Client connecté: %x %d\n", SDLNet_Read32(&ip->host), SDLNet_Read16(&ip->port));
			else
				printf("SDLNet_TCP_GetPeerAddress: %s\n", SDLNet_GetError());

            reseau->deconnection = false;
            reseau->connecte = true;
            EnvoiPaquet(cubulus, VERSION, VERSION_CLIENT, 0, 0);

			while (!reseau->stopThread && !reseau->deconnection)
			{

                paquet = malloc(sizeof(Paquet));
                paquet->suivant = NULL;
                int reception = SDLNet_TCP_Recv(reseau->serveur , paquet->tampon, TAILLE_TAMPON);
				if (reception > 0)
				{
                    if(reseau->p != NULL)
                    {
                        temp = reseau->p;
                        while(temp->suivant != NULL)
                            temp = temp->suivant;
                        temp->suivant = paquet;
                    }
                    else
                        reseau->p = paquet;
				}
				else
				{
                    free(paquet);
                    if(reception == 0)
                    {
                        reseau->erreur = -1;
                        reseau->stopThread = true;
                        reseau->deconnection = true;
                        reseau->connecte = false;
                    }
                }

			}

            SDLNet_TCP_Close(reseau->serveur );
		}
	}

	SDLNet_TCP_Close(reseau->ecoute);

	temp = reseau->p;
	Paquet* temp2;
    while(temp != NULL)
    {
        temp2 = temp;
        temp = temp->suivant;
        free(temp2);
    }

	printf("Thread serveur terminé.\n");
}

void Client(struct CubulusContext *cubulus)
{
    ReseauContext *reseau = &cubulus->reseau;
    reseau->stopThread = false;
    reseau->deconnection = false;
    reseau->erreur = 0;
	Paquet *paquet, *temp;


	if (SDLNet_ResolveHost(&reseau->ipServeur, reseau->adresse, PORT) < 0)
	{
		printf("SDLNet_ResolveHost: %s\n", SDLNet_GetError());
		reseau->erreur = -1;
		return;
	}

	reseau->serveur = SDLNet_TCP_Open(&reseau->ipServeur);
	if (!reseau->serveur)
	{
		printf("SDLNet_TCP_Open: %s\n", SDLNet_GetError());
		reseau->erreur = -1;
		return;
	}

    reseau->connecte = true;
	while (!reseau->stopThread && !reseau->deconnection)
    {
        paquet = malloc(sizeof(Paquet));
        paquet->suivant = NULL;
        int reception = SDLNet_TCP_Recv(reseau->serveur , paquet->tampon, TAILLE_TAMPON);
        if (reception > 0)
        {
            if(reseau->p != NULL)
            {
                temp = reseau->p;
                while(temp->suivant != NULL)
                    temp = temp->suivant;
                temp->suivant = paquet;
            }
            else
                reseau->p = paquet;
        }
        else
        {
            free(paquet);
            if(reception == 0)
            {
                reseau->erreur = -1;
                reseau->stopThread = true;
                reseau->deconnection = true;
                reseau->connecte = false;
            }
        }
    }

	SDLNet_TCP_Close(reseau->serveur);
	printf("Thread client terminé.\n");
}

void ReceptionPaquet(struct CubulusContext *cubulus)
{
	Paquet *paquet = cubulus->reseau.p;
	if(paquet == NULL)
        return;

	cubulus->reseau.p = paquet->suivant;
	int i;

	for(i = 0 ; i < TAILLE_TAMPON ; i++)
        paquet->tampon[i]-='0';


	switch(paquet->tampon[0])
	{

		case JOUER:

			Jouer(&cubulus->cubulus, paquet->tampon[1], paquet->tampon[2], false);

		break;

		case ROTATION:

			RotationCube(&cubulus->cubulus, paquet->tampon[1]);
			switch(paquet->tampon[1])
			{
                case HAUT :
                    cubulus->angleYCamera-=90;
                break;

                case BAS :
                    cubulus->angleYCamera+=90;
                break;

                case DROITE :
                    cubulus->angleZCamera+=90;
                break;

                case GAUCHE :
                    cubulus->angleZCamera-=90;
                break;
			}
        case TIRAGE:

            switch(paquet->tampon[1])
            {
                case BLANC:
                    cubulus->cubulus.couleurDebut = BLANC;
                    break;
                case NOIR:
                    cubulus->cubulus.couleurDebut = NOIR;
                    break;
                case ROUGE:
                    cubulus->cubulus.couleurDebut = ROUGE;
                    break;
                default:
                    printf("Erreur couleur tirage reseau.\n");
                    cubulus->reseau.deconnection = true;
                    cubulus->reseau.stopThread = true;
                    break;
            }

            break;
        case VERSION:
            if(paquet->tampon[1] != VERSION_CLIENT)
            {
                cubulus->reseau.deconnection = true;
                cubulus->reseau.stopThread = true;
                cubulus->reseau.erreur = -2;
            }
            break;

		break;

		default :

			printf("Erreur de paquet\n");

	}

	free(paquet);


}

bool EnvoiPaquet(CubulusContext *cubulus, enum Fonctions action, int paraJRL, int paraJL, int paraL)
{

	char tampon[TAILLE_TAMPON];

	int len;

	switch(action)
	{

		case JOUER :
			sprintf(tampon, "%d%d%d", 1, paraJRL, paraJL);
		break;

		case ROTATION :
			sprintf(tampon, "%d%d",2, paraJRL);
		break;

		case VERSION :
			sprintf(tampon, "%d%d",0, paraJRL);
		break;

		case TIRAGE :
			sprintf(tampon, "%d%d",3, paraJRL);
		break;

		default :
			printf("Action non reconnue !");
			break;

	}

	len = strlen(tampon) + 1;

	if (SDLNet_TCP_Send(cubulus->reseau.serveur, (void *)tampon, len) < len)
	{
		printf("SDLNet_TCP_Send: %s\n", SDLNet_GetError());
		cubulus->reseau.erreur = -1;
		return false;

	}


	return true;

}

bool ATonTour(CubulusContext* cubulus)
{
    if(cubulus->enReseau)
    {
        if(strcmp(cubulus->reseau.adresse, ADRESSE_VIDE) == 0)  //Si on est serveur
        {
            if(cubulus->cubulus.joueurs[0].aTonTour)
                return true;
            else
                return false;
        }

        else
        {
            if(cubulus->cubulus.joueurs[1].aTonTour)
                return true;
            else
                return false;
        }
    }

    return false;
}
