/**
 * \file MoteurJeu.c
 * \brief Contient les implémentations des différrentes fonctions du module du moteur de vidéo,
 *		  ce module utilise openCV
 */

#include "MoteurVideo.h"


void LanceKinect(struct CubulusContext *cubulus)
{
    cubulus->opencv.detection = false;
	cubulus->opencv.stopThread = false;

    if(cubulus->opencv.thread == NULL)
    {
        cubulus->opencv.capture = cvCreateCameraCapture(CV_CAP_ANY);

        if(!cubulus->opencv.capture) // Si la caméra n'est pas reconnu
        {
            printf("Pas de périphérique vidéo détecté.\n");
            return;
        }
        else
            cubulus->opencv.thread = SDL_CreateThread((SDL_ThreadFunction)DetectionObjet, NULL, (void*)cubulus);
    }
	cubulus->opencv.detection = false;
	cubulus->opencv.stopThread = false;
}
void StopKinect(struct CubulusContext *cubulus)
{
    if(cubulus->opencv.capture != NULL)
        cvReleaseCapture(&cubulus->opencv.capture);
    if(cubulus->opencv.thread != NULL)
        cubulus->opencv.stopThread = true;
}

void DetectionObjet(struct CubulusContext *cubulus)
{
    //Max H-S-V -> 180 - 255 - 255
    int hMin = 34, hMax=72, sMin = 58, sMax = 200, vMin = 147, vMax = 255;

    const int nbPixelTolerance = 1000;
    IplImage *masque, *image, *imageHSV;
    IplConvKernel *kernel;
    kernel = cvCreateStructuringElementEx(5, 5, 2, 2, CV_SHAPE_ELLIPSE, NULL);
    int x, y, sommeX, sommeY, nbPixels;
    int lastX = 0, lastY = 0;

    #ifdef HSV_DEBUG

        cvNamedWindow( "Test1", CV_WINDOW_AUTOSIZE );
        cvNamedWindow( "Test2", CV_WINDOW_AUTOSIZE );
        cvNamedWindow("Control", CV_WINDOW_AUTOSIZE);

        cvCreateTrackbar("hMin", "Control", &hMin, 179, NULL);
        cvCreateTrackbar("hMax", "Control", &hMax, 179, NULL);

        cvCreateTrackbar("sMin", "Control", &sMin, 255, NULL);
        cvCreateTrackbar("sMax", "Control", &sMax, 255, NULL);

        cvCreateTrackbar("vMin", "Control", &vMin, 255, NULL);
        cvCreateTrackbar("vMax", "Control", &vMax, 255, NULL);

    #endif // HSV_DEBUG

    while(!cubulus->opencv.stopThread)
    {
        sommeX = 0;
        sommeY = 0;
        nbPixels = 0;

        //Traitements de l'image
        image = cvQueryFrame( cubulus->opencv.capture );
        imageHSV = cvCloneImage(image);
        cvCvtColor(image, imageHSV, CV_BGR2HSV);
        masque = cvCreateImage(cvGetSize(image), image->depth, 1);
        cvInRangeS(imageHSV, cvScalar(hMin, sMin, vMin, 0), cvScalar(hMax, sMax, vMax, 255), masque);

        cvDilate(masque, masque, kernel, 1);
        cvErode(masque, masque, kernel, 1);
        cvDilate(masque, masque, kernel, 1);
        cvErode(masque, masque, kernel, 1);

        #ifdef HSV_DEBUG
            cvShowImage( "Test1", image );
            cvShowImage( "Test2", masque );
            if ( (cvWaitKey(10) & 255) == 27 ) break;
        #endif


        //Recherche de l'objet
        for(x = 0; x < masque->width; x++)
        {
            for(y = 0; y < masque->height; y++)
            {
                if(((uchar *)(masque->imageData + y*masque->widthStep))[x] == 255)
                {
                    sommeX += x;
                    sommeY += y;
                    nbPixels++;
                }
            }
        }

        if(nbPixels != 0)
        {
            sommeX /= nbPixels;
            sommeY /= nbPixels;
        }

        if(nbPixels >= nbPixelTolerance)
        {
            if(cubulus->opencv.detection)
            {
                cubulus->angleZCamera -= sommeX-lastX;
                cubulus->angleYCamera += sommeY-lastY;
            }

            cubulus->opencv.detection = true;
            lastX = sommeX;
            lastY = sommeY;
        }
        else
            cubulus->opencv.detection = false;




        cvReleaseImage(&masque);
        cvReleaseImage(&imageHSV);
    }
    cubulus->opencv.detection = false;

    cvReleaseStructuringElement(&kernel);
    cvReleaseCapture(&cubulus->opencv.capture);

    #ifdef HSV_DEBUG
    cvDestroyWindow( "Test1" );
    cvDestroyWindow( "Test2" );
    cvDestroyWindow( "Control" );
    #endif

    cubulus->opencv.thread = NULL;

}
