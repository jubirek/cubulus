#ifndef MOTEURVIDEO_H
#define MOTEURVIDEO_H

/**
 * \file MoteurJeu.h
 * \brief Contient les prototypes des différrentes fonctions du module du moteur de vidéo,
 *		  ce module utilise openCV
 */

struct OpenCVContext;

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <SDL2/SDL_thread.h>
#include <stdbool.h>

/**
 * \struct OpenCVContext
 * \brief Structure pour l'utilisation de la Kinect et de la mise en place du Thread
*/
struct OpenCVContext
{
    CvCapture* capture;
    bool detection;
    SDL_Thread *thread;
    bool stopThread;
};
typedef struct OpenCVContext OpenCVContext;

#include "MoteurGraphique.h"


/**
 * \fn LanceKinect(CubulusContext *cubulus)
 * \brief Gère l'utilisation de la caméra
 * \param cubulus Structure CubulusContext
 */
void LanceKinect(struct CubulusContext *cubulus);

/**
 * \fn StopKinect(CubulusContext *cubulus)
 * \brief Stope l'utilisation de la caméra
 * \param cubulus Structure CubulusContext
 */
void StopKinect(struct CubulusContext *cubulus);

/**
 * \fn DetectionObjet(CubulusContext *cubulus)
 * \brief Détecte l’objet recherché et ses mouvements sur le flux envoyé par la webcam
 * \param cubulus Structure CubulusContext
 */
void DetectionObjet(struct CubulusContext *cubulus);


#endif

