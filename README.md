 ██████╗██╗   ██╗██████╗ ██╗   ██╗██╗     ██╗   ██╗███████╗
██╔════╝██║   ██║██╔══██╗██║   ██║██║     ██║   ██║██╔════╝
██║     ██║   ██║██████╔╝██║   ██║██║     ██║   ██║███████╗
██║     ██║   ██║██╔══██╗██║   ██║██║     ██║   ██║╚════██║
╚██████╗╚██████╔╝██████╔╝╚██████╔╝███████╗╚██████╔╝███████║
 ╚═════╝ ╚═════╝ ╚═════╝  ╚═════╝ ╚══════╝ ╚═════╝ ╚══════╝
                                                
Version 2.x


* Dependencies
	- libsdl2-dev
	- libsdl2-ttf-dev
	- libsdl2-image-dev
	- libsdl2-net-dev
	- libsdl2-miwer-dev
	- freeglut3-dev 
	- libopencv-dev

 
 * Minimum pour execution :
	- libsdl2-2.0-0
	- libsdl2-image-2.0-0
	- libsdl2-ttf-2.0-0
	- libsdl2-net-2.0-0
	- libsdl2-mixer-2.0-0
	- freeglut3
	- libopencv-dev


