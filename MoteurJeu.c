#include "MoteurJeu.h"

/**
 * \file MoteurJeu.c
 * \brief Contient les différrentes fonctions du module du moteur jeu,
 *		  ce module utilise des librairies standard pour faire fonctionner le cubulus.
 */

void VideCube(Cubulus* cubulus)
{
	int i, j, k;

	for(i = 0 ; i < TAILLE_CUBE ; i++)
        for(j = 0 ; j < TAILLE_CUBE ; j++)
            for(k = 0 ; k < TAILLE_CUBE ; k++)
            {
                cubulus->tableau[i][j][k].couleur = VOID;
                cubulus->historique[i][j][k].couleur = VOID;
            }

    cubulus->billeRestante[0] = TAILLE_CUBE*TAILLE_CUBE;
    cubulus->billeRestante[1] = TAILLE_CUBE*TAILLE_CUBE;
    cubulus->billeRestante[2] = TAILLE_CUBE*TAILLE_CUBE;
    cubulus->gagne = false;


}

bool ParametrerCube(Cubulus* cubulus, Joueur j1, Joueur j2, Joueur j3)
{
    VideCube(cubulus);
    cubulus->joueurs[0].aTonTour = j1.aTonTour;
    cubulus->joueurs[1].aTonTour = j2.aTonTour;
    cubulus->joueurs[2].aTonTour = j3.aTonTour;

    cubulus->joueurs[0].joueurReseau = j1.joueurReseau;
    cubulus->joueurs[1].joueurReseau = j2.joueurReseau;
    cubulus->joueurs[2].joueurReseau = j3.joueurReseau;

    cubulus->joueurs[0].joueur = j1.joueur;
    cubulus->joueurs[1].joueur = j2.joueur;
    cubulus->joueurs[2].joueur = j3.joueur;

    cubulus->joueurs[0].niveauIA = j1.niveauIA;
    cubulus->joueurs[1].niveauIA = j2.niveauIA;
    cubulus->joueurs[2].niveauIA = j3.niveauIA;

    if(j3.couleur == VOID)
    {
        cubulus->joueurs[2].aTonTour =false;
        cubulus->troisJoueur = false;
    }
    else
        cubulus->troisJoueur = true;

    cubulus->joueurs[0].couleur = NOIR;
    cubulus->joueurs[1].couleur = ROUGE;
    cubulus->joueurs[2].couleur = BLANC;


    return true;

}

enum Couleur VerifieFace(Sphere tableau[TAILLE_CUBE][TAILLE_CUBE])
{
	int x,y;
	enum Couleur couleur=VOID, couleurVerif=VOID;

	for(x = 0 ; x < TAILLE_CUBE ; x++)
		for(y = 0 ; y < TAILLE_CUBE ; y++)
		{
			couleurVerif = tableau[x][y].couleur;
			//Condition du carré rapproché
			if ((x<2) && (y<2) && (tableau[x+1][y].couleur == couleurVerif) && (tableau[x+1][y+1].couleur == couleurVerif) && (tableau[x][y+1].couleur == couleurVerif))
				couleur = tableau[x][y].couleur;

			//Condition du grand carré
			if (((x==0) && (y==0)) && (tableau[x+2][y].couleur == couleurVerif) && (tableau[x][y+2].couleur == couleurVerif) && (tableau[x+2][y+2].couleur == couleurVerif))
				couleur = tableau[x][y].couleur;

			//Condition du losange
			if ((y==1) && (x==0) && (tableau[x+1][y+1].couleur == couleurVerif) && (tableau[x+1][y-1].couleur == couleurVerif) && (tableau[x+2][y].couleur == couleurVerif))
				couleur = tableau[x][y].couleur;

			if(couleur != VOID)
				return couleur;
		}

	return VOID;
}

enum Couleur VerifieCube(Cubulus* cubulus)
{
	int i;

	Sphere face[2][TAILLE_CUBE][TAILLE_CUBE];

	enum Couleur couleur=VOID;

	for(i = 0 ; i < 6 ; i++)
	{
		FaceCube(cubulus, face, i, false);
		couleur = VerifieFace(face[0]);
		if((couleur==BLANC)&&(!cubulus->troisJoueur))
			couleur = VOID;

		if(couleur != VOID)
		{
            cubulus->gagne = true;
			return couleur;
        }

	}

	return VOID;
}


void FaceCube(Cubulus *cubulus, Sphere tab[2][TAILLE_CUBE][TAILLE_CUBE], enum Face face, bool ecrire)
{
    int x, y, z;

    switch(face)
    {
        case F_DESSUS:
            for(x = 0 ; x < TAILLE_CUBE ; x++)
                for(y = 0 ; y < TAILLE_CUBE ; y++)
                {
                    if(ecrire)
                    {
                        cubulus->tableau[x][y][0].couleur       = tab[0][x][y].couleur;
                        cubulus->historique[x][y][1].couleur = tab[1][x][y].couleur;
                    }
                    else
                    {
                        tab[0][x][y].couleur = cubulus->tableau[x][y][0].couleur;
                        tab[1][x][y].couleur = cubulus->historique[x][y][0].couleur;
                    }
                }
            break;
        case F_DESSOUS:
            for(x = 0 ; x < TAILLE_CUBE ; x++)
                for(y = 0 ; y < TAILLE_CUBE ; y++)
                {
                    if(ecrire)
                    {
                        cubulus->tableau[x][y][TAILLE_CUBE-1].couleur       = tab[0][TAILLE_CUBE-x-1][y].couleur;
                        cubulus->historique[x][y][TAILLE_CUBE-1].couleur = tab[1][TAILLE_CUBE-x-1][y].couleur;
                    }
                    else
                    {
                        tab[0][TAILLE_CUBE-x-1][y].couleur = cubulus->tableau[x][y][TAILLE_CUBE-1].couleur;
                        tab[1][TAILLE_CUBE-x-1][y].couleur = cubulus->historique[x][y][TAILLE_CUBE-1].couleur;
                    }
                }
            break;
        case F_ARRIERE:
            for(y = 0 ; y < TAILLE_CUBE ; y++)
                for(z = 0 ; z < TAILLE_CUBE ; z++)
                {
                    if(ecrire)
                    {
                        cubulus->tableau[0][y][z].couleur       = tab[0][z][TAILLE_CUBE-y-1].couleur;
                        cubulus->historique[0][y][z].couleur = tab[1][z][TAILLE_CUBE-y-1].couleur;
                    }
                    else
                    {
                        tab[0][z][TAILLE_CUBE-y-1].couleur = cubulus->tableau[0][y][z].couleur;
                        tab[1][z][TAILLE_CUBE-y-1].couleur = cubulus->historique[0][y][z].couleur;
                    }
                }
            break;
        case F_FACE:
            for(y = 0 ; y < TAILLE_CUBE ; y++)
                for(z = 0 ; z < TAILLE_CUBE ; z++)
                {
                    if(ecrire)
                    {
                        cubulus->tableau[TAILLE_CUBE-1][z][y].couleur       = tab[0][y][z].couleur;
                        cubulus->historique[TAILLE_CUBE-1][z][y].couleur = tab[1][y][z].couleur;
                    }
                    else
                    {
                        tab[0][y][z].couleur = cubulus->tableau[TAILLE_CUBE-1][z][y].couleur;
                        tab[1][y][z].couleur = cubulus->historique[TAILLE_CUBE-1][z][y].couleur;
                    }
                }
            break;
        case F_GAUCHE:
            for(x = 0 ; x < TAILLE_CUBE ; x++)
                for(z = 0 ; z < TAILLE_CUBE ; z++)
                {
                    if(ecrire)
                    {
                        cubulus->tableau[x][0][z].couleur       = tab[0][z][x].couleur;
                        cubulus->historique[x][0][z].couleur = tab[1][z][x].couleur;
                    }
                    else
                    {
                        tab[0][z][x].couleur = cubulus->tableau[x][0][z].couleur;
                        tab[1][z][x].couleur = cubulus->historique[x][0][z].couleur;
                    }
                }
            break;
        case F_DROITE:
            for(x = 0 ; x < TAILLE_CUBE ; x++)
                for(z = 0 ; z < TAILLE_CUBE ; z++)
                {
                    if(ecrire)
                    {
                        cubulus->tableau[x][TAILLE_CUBE-1][z].couleur       = tab[0][z][TAILLE_CUBE-x-1].couleur;
                        cubulus->historique[x][TAILLE_CUBE-1][z].couleur = tab[1][z][TAILLE_CUBE-x-1].couleur;
                    }
                    else
                    {
                        tab[0][z][TAILLE_CUBE-x-1].couleur = cubulus->tableau[x][TAILLE_CUBE-1][z].couleur;
                        tab[1][z][TAILLE_CUBE-x-1].couleur = cubulus->historique[x][TAILLE_CUBE-1][z].couleur;
                    }
                }
            break;
        case F_ARRIERE_INVERSE:
            for(y = 0 ; y < TAILLE_CUBE ; y++)
                for(z = 0 ; z < TAILLE_CUBE ; z++)
                {
                    if(ecrire)
                    {
                        cubulus->tableau[0][y][z].couleur       = tab[0][TAILLE_CUBE-z-1][y].couleur;
                        cubulus->historique[0][y][z].couleur = tab[1][TAILLE_CUBE-z-1][y].couleur;
                    }
                    else
                    {
                        tab[0][TAILLE_CUBE-z-1][y].couleur = cubulus->tableau[0][y][z].couleur;
                        tab[1][TAILLE_CUBE-z-1][y].couleur = cubulus->historique[0][y][z].couleur;
                    }
                }
            break;
	case F_MILIEU:
            for(x = 0 ; x < TAILLE_CUBE ; x++)
                for(y = 0 ; y < TAILLE_CUBE ; y++)
                {
                    if(ecrire)
                    {
                        cubulus->tableau[x][y][TAILLE_CUBE/2].couleur       = tab[0][x][y].couleur;
                        cubulus->historique[x][y][TAILLE_CUBE/2].couleur = tab[1][x][y].couleur;
                    }
                    else
                    {
                        tab[0][x][y].couleur = cubulus->tableau[x][y][TAILLE_CUBE/2].couleur;
                        tab[1][x][y].couleur = cubulus->historique[x][y][TAILLE_CUBE/2].couleur;
                    }
                }
            break;
        default:
            printf("Erreur FACE : %d\n", face);
            break;
    }
}


int AjouteBouleCote(Sphere face[TAILLE_CUBE][TAILLE_CUBE], enum Direction entree, enum Couleur newCouleur, int rang, Cubulus *cubulus)
{
	int x,y;
	enum Couleur aux;

	switch(entree)
	{
		// Initialisation des variables en fonction de l'entrée
		case DROITE:

			x=TAILLE_CUBE-1;
			y=rang;

            if(EstRemplis(cubulus) == false)
			{
				// S'il y a une boule au contact, on décale cette boule avec notre boule à poser dans le sens décroissant des axes
				if(((face[y][x-2].couleur == VOID) && (face[y][x-1].couleur == VOID) && (face[y][x].couleur != VOID))
					|| ((face[y][x-2].couleur != VOID) && (face[y][x-1].couleur == VOID) && (face[y][x].couleur != VOID)))
				{
					face[y][x-1].couleur = face[y][x].couleur;
					face[y][x].couleur = newCouleur;

					return 1;
				}


				// S'il ya deux boules alignées sur la rangée, on décale ces deux boules avec notre boule à poser dans le sens décroissant des axes
				if((face[y][x-2].couleur == VOID) && (face[y][x-1].couleur != VOID) && (face[y][x].couleur != VOID))
				{
					face[y][x-2].couleur = face[y][x-1].couleur;
					face[y][x-1].couleur = face[y][x].couleur;
					face[y][x].couleur = newCouleur;

					return 1;
				}

				// S'il n'y a pas de boule à l'endroit où on veut poser la boule, on la pose.
				if (face[x][y].couleur == VOID)
				{
					face[x][y].couleur = newCouleur;

					return 1;
				}
            }

            if((face[y][x-2].couleur != VOID) && (face[y][x-1].couleur != VOID) && (face[y][x].couleur != VOID))
            {
                //Si je change l'ordre à droite je décale dans le sens décroissant des axes
                aux=face[rang][x-2].couleur;
                face[rang][x-2].couleur = face[rang][x-1].couleur;
                face[rang][x-1].couleur = face[rang][x].couleur;
                face[rang][x].couleur = aux;

                return 2;
            }

		break;

		case GAUCHE:

			x=rang;
			y=0;

			if (EstRemplis(cubulus) == false)
			{

				// S'il y a une boule au contact, on décale cette boule avec notre boule à poser dans le sens croissant des axes
				if(((face[x][y].couleur != VOID) && (face[x][y+1].couleur == VOID) && (face[x][y+2].couleur == VOID))
					|| ((face[x][y].couleur != VOID) && (face[x][y+1].couleur == VOID) && (face[x][y+2].couleur != VOID)))
				{
					face[x][y+1].couleur = face[x][y].couleur;
					face[x][y].couleur = newCouleur;

					return 1;
				}

				// S'il ya deux boules alignées sur la rangée, on décale ces deux boules avec notre boule à poser dans le sens croissant des axes
				if((face[x][y].couleur != VOID) && (face[x][y+1].couleur != VOID) && (face[x][y+2].couleur == VOID))
				{
					face[x][y+2].couleur = face[x][y+1].couleur;
					face[x][y+1].couleur = face[x][y].couleur;
					face[x][y].couleur = newCouleur;

					return 1;
				}

				// S'il n'y a pas de boule à l'endroit où on veut poser la boule, on la pose.
				if (face[x][y].couleur == VOID)
				{
					face[x][y].couleur = newCouleur;

					return 1;
				}
            }

            if((face[x][y].couleur != VOID) && (face[x][y+1].couleur != VOID) && (face[x][y+2].couleur == VOID))
            {
                aux=face[rang][y+2].couleur;
                face[rang][y+2].couleur = face[rang][y+1].couleur;
                face[rang][y+1].couleur = face[rang][y].couleur;
                face[rang][y].couleur = aux;

                return 2;
            }


		break;


		case BAS:

			x= TAILLE_CUBE-1;
			y=rang;

			if(EstRemplis(cubulus) == false)
			{

				// S'il y a une boule au contact, on décale cette boule avec notre boule à poser dans le sens croissant des axes
				if(((face[x][y].couleur != VOID) && (face[x-1][y].couleur == VOID) && (face[x-2][y].couleur == VOID))
					|| ((face[x][y].couleur != VOID) && (face[x-1][y].couleur == VOID) && (face[x-2][y].couleur != VOID)))
				{
					face[x-1][y].couleur = face[x][y].couleur;
					face[x][y].couleur = newCouleur;

					return 1;
				}

				// S'il ya deux boules alignées sur la rangée, on décale ces deux boules avec notre boule à poser dans le sens croissant des axes
				if((face[x][y].couleur != VOID) && (face[x-1][y].couleur != VOID) && (face[x-2][y].couleur == VOID))
				{
					face[x-2][y].couleur = face[x-1][y].couleur;
					face[x-1][y].couleur = face[x][y].couleur;
					face[x][y].couleur = newCouleur;

					return 1;
				}

				// S'il n'y a pas de boule à l'endroit où on veut poser la boule, on la pose.
				if (face[x][y].couleur == VOID)
				{
					face[x][y].couleur = newCouleur;

					return 1;
				}
			}

            if((face[x][y].couleur != VOID) && (face[x-1][y].couleur != VOID) && (face[x-2][y].couleur != VOID))
            {
                //Si je change l'ordre en bas je décale dans le sens décroissant des axes
                aux=face[x][y].couleur;
                face[x][y].couleur = face[x-2][y].couleur;
                face[x-2][y].couleur = face[x-1][y].couleur;
                face[x-1][y].couleur = aux;

                return 2;
            }


		break;

		case HAUT:

			x=0;
			y=rang;

			if(EstRemplis(cubulus) == false)
			{

				// S'il y a une boule au contact, on décale cette boule avec notre boule à poser dans le sens croissant des axes
				if(((face[x][y].couleur != VOID) && (face[x+1][y].couleur == VOID) && (face[x+2][y].couleur == VOID))
					|| ((face[x][y].couleur != VOID) && (face[x+1][y].couleur == VOID) && (face[x+2][y].couleur != VOID)))
				{
					face[x+1][y].couleur = face[x][y].couleur;
					face[x][y].couleur = newCouleur;

					return 1;
				}

				// S'il ya deux boules alignées sur la rangée, on décale ces deux boules avec notre boule à poser dans le sens croissant des axes
				if((face[x][y].couleur != VOID) && (face[x+1][y].couleur != VOID) && (face[x+2][y].couleur == VOID))
				{
					face[x+2][y].couleur = face[x+1][y].couleur;
					face[x+1][y].couleur = face[x][y].couleur;
					face[x][y].couleur = newCouleur;

					return 1;
				}

				// S'il n'y a pas de boule à l'endroit où on veut poser la boule, on la pose.
				if (face[x][y].couleur == VOID)
				{
					face[x][y].couleur = newCouleur;

					return 1;
				}
			}

            if((face[x][y].couleur != VOID) && (face[x+1][y].couleur != VOID) && (face[x+2][y].couleur != VOID))
            {
                //Si je change l'ordre en haut je décale dans le sens croissant des axes
                aux=face[x][y].couleur;
                face[x][y].couleur = face[x+2][y].couleur;
                face[x+2][y].couleur = face[x+1][y].couleur;
                face[x+1][y].couleur = aux;

                return 2;
            }


		break;
	}

	return 0;

}

bool AjouterBoule(Cubulus *cubulus, int x, int y, enum Couleur couleurJ)
{
    enum Couleur c = 0;
    bool ok = false;
    int retour;
    Sphere face[2][TAILLE_CUBE][TAILLE_CUBE];

    if(cubulus->gagne)
        return false;

    if(x < 1 || x > TAILLE_CUBE)
         if(y < 1 || y > TAILLE_CUBE)
            return false;

    c = couleurJ;

    if(!cubulus->troisJoueur && cubulus->billeRestante[BLANC] > 0)
        c = BLANC;


    if(y == 1)
    {
        FaceCube(cubulus, face, F_DESSUS, false);
        retour=AjouteBouleCote(face[0], BAS, c, x-1, cubulus);
        if(retour)
        {
            if(cubulus->billeRestante[c] > 0 && retour == 1)
                cubulus->billeRestante[c]--;
			ok = true;
        }
		FaceCube(cubulus, face, F_DESSUS, true);
    }
    else if(y == 2)
    {
        FaceCube(cubulus, face, F_MILIEU, false);
        retour=AjouteBouleCote(face[0], BAS, c, x-1, cubulus);
        if(retour)
        {
            if(cubulus->billeRestante[c] > 0 && retour == 1)
                cubulus->billeRestante[c]--;
        	ok = true;
        }
        FaceCube(cubulus, face, F_MILIEU, true);
    }
    else if(y == 3)
    {
        FaceCube(cubulus, face, F_DESSOUS, false);
        retour=AjouteBouleCote(face[0], HAUT, c, x-1, cubulus);
        if(retour)
        {
            if(cubulus->billeRestante[c] > 0 && retour == 1)
                cubulus->billeRestante[c]--;
        	ok = true;
        }
        FaceCube(cubulus, face, F_DESSOUS, true);
    }
    return ok;

}

void RotationCube(Cubulus* cubulus, enum Direction direction)
{
    Sphere tableauFace[7][2][TAILLE_CUBE][TAILLE_CUBE];
    int i;

        for(i = 0 ; i < 7 ; i++)
            FaceCube(cubulus, tableauFace[i], i, false);


        switch(direction)
        {
            case DROITE:
                FaceCube(cubulus, tableauFace[F_DROITE], F_FACE, true);
                FaceCube(cubulus, tableauFace[F_FACE], F_GAUCHE, true);
                FaceCube(cubulus, tableauFace[F_GAUCHE], F_ARRIERE, true);
                FaceCube(cubulus, tableauFace[F_ARRIERE], F_DROITE, true);
                break;
            case GAUCHE:
                FaceCube(cubulus, tableauFace[F_GAUCHE], F_FACE, true);
                FaceCube(cubulus, tableauFace[F_ARRIERE], F_GAUCHE, true);
                FaceCube(cubulus, tableauFace[F_DROITE], F_ARRIERE, true);
                FaceCube(cubulus, tableauFace[F_FACE], F_DROITE, true);
                break;
            case BAS:
                FaceCube(cubulus, tableauFace[F_DESSOUS], F_FACE, true);
                FaceCube(cubulus, tableauFace[F_FACE], F_DESSUS, true);
                FaceCube(cubulus, tableauFace[F_DESSUS], F_ARRIERE_INVERSE, true);
                FaceCube(cubulus, tableauFace[F_ARRIERE_INVERSE], F_DESSOUS, true);
                break;
            case HAUT:
                FaceCube(cubulus, tableauFace[F_DESSUS], F_FACE, true);
                FaceCube(cubulus, tableauFace[F_FACE], F_DESSOUS, true);
                FaceCube(cubulus, tableauFace[F_DESSOUS], F_ARRIERE_INVERSE, true);
                FaceCube(cubulus, tableauFace[F_ARRIERE_INVERSE], F_DESSUS, true);
                break;
            default:
                break;
        }


}

bool EstRemplis(Cubulus *cubulus)
{
    int i, j, k;
    for(i = 0 ; i < TAILLE_CUBE ; i++)
        for(j = 0 ; j < TAILLE_CUBE ; j++)
            for(k = 0 ; k < TAILLE_CUBE ; k++)
                if(cubulus->tableau[i][j][k].couleur == VOID)
                    return false;
    return true;
}

bool Jouer(Cubulus *cubulus, int x, int y)
{
    int i;
    Cubulus test;
    Copie(cubulus, &test);
    Sphere memoire[TAILLE_CUBE][TAILLE_CUBE][TAILLE_CUBE];
    CopieTableau3D(cubulus->tableau, memoire);

    for(i = 0 ; i < 3 ; i++)
        if(cubulus->joueurs[i].aTonTour)
        {
            if(!AjouterBoule(&test, x, y, cubulus->joueurs[i].couleur))
                return false;
            if(CompareTableau3D(test.tableau, cubulus->historique))
                return false;
            else
                Copie(&test, cubulus);

            CopieTableau3D(memoire, cubulus->historique);

            if(cubulus->billeRestante[BLANC] <= 0 || cubulus->troisJoueur)
            {
                cubulus->joueurs[i].aTonTour = false;
                int suivant;
                if(cubulus->troisJoueur)
                    suivant = (i+1)%3;
                else
                    suivant = (i+1)%2;

                cubulus->joueurs[suivant].aTonTour = true;
            }

            break;
        }

        return true;
}

void Copie(Cubulus *cubulus1, Cubulus *cubulus2)
{
    int i, j, k;
    VideCube(cubulus2);
    for(i = 0 ; i < TAILLE_CUBE ; i++)
        for(j = 0 ; j < TAILLE_CUBE ; j++)
            for(k = 0 ; k < TAILLE_CUBE ; k++)
            {
                cubulus2->tableau[i][j][k].couleur = cubulus1->tableau[i][j][k].couleur;
                cubulus2->historique[i][j][k].couleur = cubulus1->historique[i][j][k].couleur;
            }
    cubulus2->gagne = cubulus1->gagne;
    cubulus2->troisJoueur = cubulus1->troisJoueur;

    for(i = 0 ; i < 3 ; i++)
    {
        cubulus2->billeRestante[i] = cubulus1->billeRestante[i];
        cubulus2->joueurs[i].aTonTour = cubulus1->joueurs[i].aTonTour;
        cubulus2->joueurs[i].couleur = cubulus1->joueurs[i].couleur;
        cubulus2->joueurs[i].joueur = cubulus1->joueurs[i].joueur;
        cubulus2->joueurs[i].niveauIA = cubulus1->joueurs[i].niveauIA;
        cubulus2->joueurs[i].joueurReseau = cubulus1->joueurs[i].joueurReseau;
    }
}
void CopieTableau3D(Sphere tab1[TAILLE_CUBE][TAILLE_CUBE][TAILLE_CUBE], Sphere tab2[TAILLE_CUBE][TAILLE_CUBE][TAILLE_CUBE])
{
    int i, j, k;
    for(i = 0 ; i < TAILLE_CUBE ; i++)
        for(j = 0 ; j < TAILLE_CUBE ; j++)
            for(k = 0 ; k < TAILLE_CUBE ; k++)
                tab2[i][j][k].couleur = tab1[i][j][k].couleur;

}
bool CompareTableau3D(Sphere tab1[TAILLE_CUBE][TAILLE_CUBE][TAILLE_CUBE], Sphere tab2[TAILLE_CUBE][TAILLE_CUBE][TAILLE_CUBE])
{
    int i, j, k;
    for(i = 0 ; i < TAILLE_CUBE ; i++)
        for(j = 0 ; j < TAILLE_CUBE ; j++)
            for(k = 0 ; k < TAILLE_CUBE ; k++)
                if(tab1[i][j][k].couleur != tab2[i][j][k].couleur)
                    return false;
    return true;

}

//!\ Ne marche que si c'est au tour de la couleur de jouer

int EvalueCube(Cubulus *cubulus, enum Couleur couleurIA, int* pX, int* pY, int* pFace, int etage, int etageMax)
{
    int face;
    int x, y, meilleurGain = -1458000; // Il faut initialisé le meilleurGain à un gain extrême, pour choisir ensuite un gain supérieur
    int gain = 0, sommeGain = 0;
    bool nulle = true;

    Cubulus nouveauCube;
    Copie(cubulus, &nouveauCube);
    enum Couleur couleurGain = VerifieCube(&nouveauCube);

    if(couleurGain == couleurIA)
    {
        nouveauCube.gagne = true;
        if(etage <= 1)
            return 1500;
        return (etageMax+1 - etage)*5;
    }
    else if(couleurGain != VOID)
    {
        if(cubulus->troisJoueur)
        {
            nouveauCube.gagne = true;
            if(etage == 2 || etage == 3)
                return -500;
            return -(etageMax+1 - etage)*10;
        }
        else if(couleurGain != BLANC)
        {
            nouveauCube.gagne = true;
            if(etage == 2)
                return -500;
            return -(etageMax+1 - etage)*10;
        }
    }

    if(etage >= etageMax)
        return 0;

    /* Si le cube n'est pas gagnant
     * alors on effectue tout les coups possibles
     * pour trouver celui qui donne un meilleur 'gain'
     */

    Cubulus cubeEnfant;
    for(face = 0 ; face < 6 ; face++)
    {
        for(x = 1 ; x <= TAILLE_CUBE ; x++)
        {
            for(y = 1 ; y <= TAILLE_CUBE ; y++)
            {
                Copie(&nouveauCube, &cubeEnfant);
                if(Jouer(&cubeEnfant, x, y))
                {
                    gain = EvalueCube(&cubeEnfant, couleurIA, NULL, NULL, NULL, etage+1, etageMax);
                    sommeGain += gain;
                    if(gain != 0 && nulle)
                        nulle = false;
                    if(etage == 0 && gain > meilleurGain)
                    {
                        *pX = x;
                        *pY = y;
                        *pFace = face;
                        meilleurGain = gain;
                    }
                }
            }
        }
        switch(face)
        {
            case 0:
            case 1:
            case 2:
                RotationCube(&nouveauCube, DROITE);
                break;
            case 3:
                RotationCube(&nouveauCube, DROITE);
                RotationCube(&nouveauCube, HAUT);
                break;
            case 4:
                RotationCube(&nouveauCube, BAS);
                RotationCube(&nouveauCube, BAS);
                break;
            default:
                break;
        }
    }


    if(nulle && etage == 0)                         //Si aucun gain ne se démarque des autres, on joue aléatoirement
    {
        do
        {
            Copie(cubulus, &nouveauCube);
            *pX = (rand() % TAILLE_CUBE)+1;
            *pY = (rand() % TAILLE_CUBE)+1;
            *pFace = (rand() % 6);
            switch(*pFace)
            {
            case 1:
                RotationCube(&nouveauCube, DROITE);
                break;
            case 2:
                RotationCube(&nouveauCube, DROITE);
                RotationCube(&nouveauCube, DROITE);
                break;
            case 3:
                RotationCube(&nouveauCube, GAUCHE);
                break;
            case 4:
                RotationCube(&nouveauCube, HAUT);
                break;
            case 5:
                RotationCube(&nouveauCube, BAS);
                break;
            default:
                break;
            }
        }while(!Jouer(&nouveauCube, *pX, *pY) && !cubulus->gagne);
    }

    return sommeGain;

}



enum Face IAJoue(Cubulus *cubulus)
{
    Joueur *ia = NULL;
    Cubulus copieCubulus;
    int x, y, face, i;
    for(i = 0 ; i < 3 ; i++)
        if(cubulus->joueurs[i].aTonTour && !cubulus->joueurs[i].joueur)
            ia = &cubulus->joueurs[i];

    if(ia == NULL)
    {
        printf("Erreur 01 : IA introuvable\n");
        return 0;
    }

    Copie(cubulus, &copieCubulus);
    EvalueCube(&copieCubulus, ia->couleur, &x, &y, &face, 0, ia->niveauIA);

    switch(face)
    {
        case 0:
            face = F_FACE;
            break;
        case 1:
            RotationCube(cubulus, DROITE);
            face = F_DROITE;
            break;
        case 2:
            RotationCube(cubulus, DROITE);
            RotationCube(cubulus, DROITE);
            face = F_ARRIERE;
            break;
        case 3:
            RotationCube(cubulus, GAUCHE);
            face = F_GAUCHE;
            break;
        case 4:
            RotationCube(cubulus, HAUT);
            face = F_DESSUS;
            break;
        case 5:
            RotationCube(cubulus, BAS);
            face = F_DESSOUS;
            break;
        default:
            printf("Erreur 02 : réponse IA incohérente\n");
            break;
        }

    if(!Jouer(cubulus, x, y))
        printf("Erreur 03 : réponse IA impossible\n");
    return face;
}


enum Couleur TireAuSort(Cubulus *cubulus)
{
    int max = (cubulus->troisJoueur) ? 3 : 2;
    return (int)((rand()%max)+((cubulus->troisJoueur) ? 0 : 1));
}

