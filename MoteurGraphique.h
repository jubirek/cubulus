#ifndef MOTEURGRAPHIQUE_H
#define MOTEURGRAPHIQUE_H

/**
 * \file MoteurGraphique.h
 * \brief Contient les prototypes des différrentes fonctions du module du moteur graphique,
 *		  ce module utilise la SDL2, OpenGl, et OpenCv ainsi que leurs composantes.
 */

struct CubulusContext;

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL2_rotozoom.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include "MoteurJeu.h"
#include "MoteurReseau.h"
#include "MoteurVideo.h"


/**
 * \def HAUTEUR_FENETRE
 * \brief Hauteur de la fenêtre : 600 px
 */
#define HAUTEUR_FENETRE 600

/**
 * \def LARGEUR_FENETRE
 * \brief Largeur de la fenêtre : 600 px
 */
#define LARGEUR_FENETRE 600

/**
 * \def POLICETITRE
 * \brief Police utilisée pour les titres des menus : Cubic.ttf
 */
#define POLICETITRE "./data/cubic.ttf"

/**
 * \def POLICE
 * \brief Police utilisée pour tout le reste du programme : DejaVuSerif.ttf
 */
#define POLICE "./data/DejaVuSerif.ttf"

/**
 * \def RAYON_SPHERE
 * \brief Rayon des sphères du cube : 1
 */
#define RAYON_SPHERE 1

/**
 * \def PAS
 * \brief Vitesse de la rotation de caméra : 2
 */
#define PAS 2

/**
 * \struct CubulusContext
 * \brief Structure déjissant tout le contexte d'affichage (camera, kinect, fenetre, cubulus etc..)
*/
struct CubulusContext
{
    SDL_Window* fenetre;
    SDL_GLContext contexteOpenGL;
    SDL_Renderer *rendu;
    struct OpenCVContext opencv;
    TTF_Font *police;
    int angleXCamera;
    int angleYCamera;
    int angleZCamera;
    struct Cubulus cubulus;
    struct ReseauContext reseau;
    bool enReseau;
    Mix_Music *musiqueFond;
};
typedef struct CubulusContext CubulusContext;

/**
 * \struct Surface
 * \brief Structure définissant les billes du cube
*/
struct Surface
{
	SDL_Surface *surface;
	SDL_Rect position;
};
typedef struct Surface Surface;


/**
 * \fn int Initialisation(CubulusContext* cubulus)
 * \brief Initialise l’interface graphique.
 * \param cubulus Structure CubulusContext
 * \return Entier correspondant à un code d'erreur.
 */
int Initialisation(CubulusContext* cubulus); // Initialise le CubulusContext

/**
 * \fn DestroyCubulus(CubulusContext* cubulus)
 * \brief Libère la mémoire allouée par la structure CubulusContext
 * \param cubulus Structure CubulusContext
 */
void DestroyCubulus(CubulusContext* cubulus);


/**
 * \fn bool Menu(CubulusContext* cubulus)
 * \brief Affiche le menu.
 * \param cubulus Structure CubulusContext
 * \return Code correspondant une action précise à effectué, demandé par l'utilisateur
 */
int Menu(CubulusContext* cubulus);  // Menu

/**
 * \fn void Regles(CubulusContext* cubulus)
 * \brief Affiche les règles.
 * \param cubulus Structure CubulusContext
 */
void Regles(CubulusContext* cubulus); // Section règles

/**
 * \fn void Credits(CubulusContext* cubulus)
 * \brief Affiche les crédits.
 * \param cubulus Structure CubulusContext
 */
void Credits(CubulusContext* cubulus); // Section crédits

/**
 * \fn bool Options(CubulusContext* cubulus)
 * \brief Affiche le menu.
 * \param cubulus Structure CubulusContext
 * \return false s'il faut jouer, true s'il faut rester
 */
bool Options(CubulusContext* cubulus); // Section options

/**
 * \fn bool Reseau(CubulusContext* cubulus)
 * \brief Affiche le menu reseau.
 * \param cubulus Structure CubulusContext
 * \return false s'il faut jouer, true s'il faut rester
 */
bool Reseau(CubulusContext* cubulus); // Section reseau

/**
 * \fn bool FinJeu(CubulusContext* cubulus)
 * \brief Affichage de victoire.
 * \param cubulus Structure CubulusContext
 * \param victoire bool
 * \param couleur enum Couleur
 * \return false s'il faut rejouer, true s'il faut quitter
 */
bool FinJeu(CubulusContext* cubulus, bool victoire, enum Couleur couleur);

/**
 * \fn enum Couleur Tirage(CubulusContext* cubulus)
 * \brief Tirage au sort du joueur qui va jouer en premier
 * \param cubulus Structure CubulusContext
 * \return la couleur qui doit jouer
 */
enum Couleur Tirage(CubulusContext* cubulus);

/**
 * \fn void Hebergement(CubulusContext* cubulus)
 * \brief Affichage d'une page d'attente de client.
 * \param cubulus Structure CubulusContext
 * \return true si l'utilisateur veut arreter l'hébergement
 */
bool Hebergement(CubulusContext* cubulus);

/**
 * \fn bool GestionEvenements(CubulusContext* cubulus)
 * \brief Gère les évènements.
 * \param cubulus Structure CubulusContext
 * \return Code correspondant une action précise à effectué demandé par l'utilisateur
 */
int GestionEvenements(CubulusContext* cubulus);

/**
 * \fn void GestionClique(CubulusContext *cubulus, int x, int y)
 * \brief Gère les clics.
 * \param cubulus Structure CubulusContext
 * \param x Abscisse de la case cliqué
 * \param y Ordonnée de la case cliqué
 */
void GestionClique(CubulusContext *cubulus, int x, int y);


/**
 * \fn void Draw(CubulusContext *cubulus)
 * \brief Affiche l'environnement du cubulus.
 * \param cubulus Structure CubulusContext
 */
void Draw(CubulusContext* cubulus);

/**
 * \fn void DrawSphere(enum Couleur couleur)
 * \brief Dessine une bille
 * \param couleur Couleur de la sphere
 */
void DrawSphere(enum Couleur couleur);      //Dessine une sphere à la position courante

/**
 * \fn void DrawCubulus(CubulusContext *cubulus)
 * \brief Dessine le cube en fonction des billes
 * \param cubulus, Structure CubulusContext
 */
void DrawCubulus(CubulusContext* cubulus);  //Appelle DrawSphere et gère la position courante

/**
 * \fn void DrawGrille()
 * \brief Dessine la grille sur la première face du cube.
 */
void DrawGrille();


/**
 * \fn void DrawInterface(CubulusContext *cubulus)
 * \param cubulus, Structure CubulusContext
 * \brief Dessine les informations de la partie à l'écran
 */
void DrawInterface(CubulusContext *cubulus);


/**
 * \fn void LimiteFps(int ticks)
 * \brief Limite les images par seconde pour éviter une surconsommation du temps CPU
 * \param ticks entier limitant le nombre d'ips, en ms
 */
void LimiteFps(int ticks);


/**
 * \fn void ChangerCouleur(enum Couleur couleur, SDL_Surface **texte, char* intitule, TTF_Font *police)
 * \brief Change la couleur au survolage d'une section dans le menu
 * \param couleur Couleur a changer
 * \param texte Pointeur de pointeur du SDL_Surface à modifier
 * \param intitule Chaine de caractère
 * \param police Pointeur du le TTF_Font à utiliser
 */
void ChangerCouleur(enum Couleur couleur, SDL_Surface **texte, char* intitule, TTF_Font *police);


/**
 * \fn void EcrireSurOpenGL(int x, int y, char *string, void *font)
 * \brief Ecris un texte sur la partie 3D du programme
 * \param x Abscisse
 * \param y Ordonnée
 * \param string Chaine de caractère à écrire
 * \param font Police à utiliser
 */
void EcrireSurOpenGL(int x, int y, char *string, void *font);



#endif // MOTEURGRAPHIQUE_H
