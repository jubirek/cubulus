#include "MoteurGraphique.h"

/**
 * \file MoteurGraphique.c
 * \brief Contient les différrentes fonctions du module du moteur graphique,
 *		  ce module utilise la SDL2, OpenGl, et OpenCv ainsi que leurs composantes.
 */

void EcrireSurOpenGL(int x, int y, char *string, void *font)
{
	int len,i; // len donne la longueur de la chaîne de caractères

	glRasterPos2f(x,y); // Positionne le premier caractère de la chaîne
	len = (int) strlen(string); // Calcule la longueur de la chaîne
	for (i = 0; i < len; i++)
        glutBitmapCharacter(font,string[i]); // Affiche chaque caractère de la chaîne
}

void Draw(CubulusContext* cubulus)
{
    int lightPos[4] = {9,0,3,0};
	static int matSpec [4] = {1,1,1,1};

    glMaterialiv(GL_FRONT_AND_BACK,GL_SPECULAR,matSpec);
    glMateriali(GL_FRONT_AND_BACK,GL_SHININESS,100);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(1.0,1.0,1.0,0.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    gluLookAt(9,0,0,0,0,0,0,0,1);

    glLightiv(GL_LIGHT0,GL_POSITION,lightPos);

    if(cubulus->angleXCamera == 0 && cubulus->angleYCamera == 0 && cubulus->angleZCamera == 0)
    {
        glDisable(GL_LIGHTING);
        DrawGrille();
        DrawInterface(cubulus);
        glEnable(GL_LIGHTING);
    }

    glRotated(cubulus->angleZCamera,0,0,1);
    glRotated(cubulus->angleYCamera,0,1,0);
    glRotated(cubulus->angleXCamera,1,0,0);

    DrawCubulus(cubulus);

    glFlush();
    SDL_GL_SwapWindow(cubulus->fenetre);


}

void DrawSphere(enum Couleur couleur)
{
    unsigned char r = 75;
    unsigned char v = 75;
    unsigned char b = 75;
    if(couleur == VOID)
        return;
    else if(couleur == ROUGE)
        r = 200;
    else if(couleur == BLANC)
    {
        r = 200;
        v = 200;
        b = 200;
    }
	glColor3ub(r, v, b);
	GLUquadric* quadrique=gluNewQuadric();
	gluQuadricDrawStyle(quadrique,GLU_FILL);
	gluSphere(quadrique, RAYON_SPHERE, 50, 50);
	gluDeleteQuadric(quadrique);
}

void DrawCubulus(CubulusContext* cubulus)
{

	glTranslated(-2*RAYON_SPHERE,-2*RAYON_SPHERE,2*RAYON_SPHERE);
	int i, j, k;
	for(i = 0 ; i < TAILLE_CUBE ; i++)
	{
        for(j = 0 ; j < TAILLE_CUBE ; j++)
        {
            for(k = 0 ; k < TAILLE_CUBE ; k++)
            {
                DrawSphere(cubulus->cubulus.tableau[k][j][i].couleur);
                glTranslated(2*RAYON_SPHERE,0,0);
            }
            glTranslated(-3*2*RAYON_SPHERE,2*RAYON_SPHERE,0);
        }
        glTranslated(0,-3*2*RAYON_SPHERE,-2*RAYON_SPHERE);
	}
}

void DrawGrille()
{
    glColor4d(0.5f, 0.5f, 0.5f, 0.5f);
    glBegin(GL_QUADS);
        glVertex3f(3,0.90,-3);
        glVertex3f(3, 0.80,-3);
        glVertex3f(3, 0.80,3);
        glVertex3f(3,0.90,3);
    glEnd();
    glBegin(GL_QUADS);
        glVertex3f(3,-0.90,-3);
        glVertex3f(3, -0.80,-3);
        glVertex3f(3, -0.80,3);
        glVertex3f(3,-0.90,3);
    glEnd();
     glBegin(GL_QUADS);
        glVertex3f(3,-3,0.90);
        glVertex3f(3,-3 ,0.80);
        glVertex3f(3, 3,0.80);
        glVertex3f(3, 3,0.90);
    glEnd();

    glBegin(GL_QUADS);
        glVertex3f(3,-3,-0.90);
        glVertex3f(3, -3,-0.80);
        glVertex3f(3, 3,-0.80);
        glVertex3f(3,3,-0.90);
    glEnd();

}

void DrawInterface(CubulusContext *cubulus)
{
    int i;
    char texte[20];

    glTranslatef(0,-6,-6);


    if(!cubulus->cubulus.gagne)
    {
        for(i = 0 ; i < 3 ; i++)
        if(cubulus->cubulus.joueurs[i].aTonTour)
        {
            sprintf(texte, "Joueur %d", i+1);
            break;
        }
        switch(i)
        {
            case 2:
                glColor3d(0.5,0.5,0.5);
                break;
            case 0:
                glColor3d(0,0,0);
                break;
            case 1:
                glColor3d(0.9,0,0);
                break;
        }

        EcrireSurOpenGL(0, 5, texte, GLUT_BITMAP_TIMES_ROMAN_24);
    }


    glTranslatef(0,6,6);
}


int GestionEvenements(CubulusContext* cubulus)
{
    static bool boutonRight = false;
    static bool boutonLeft = false;
    static bool boutonUp = false;
    static bool boutonDown = false;
    static bool boutonShift = false;

    static int coordClic[2] = {0, 0};
    static bool mouvementSouris = false;
    static bool clique = false;

    int retour = 0;

    int i;

    if(cubulus->enReseau) // Si le cubulus est en réseau
    {
        if(cubulus->reseau.erreur != 0) // Qu'il y a une erreur
        {
            retour = 1;
            cubulus->reseau.stopThread = true; //On stop le Thread Serveur
            cubulus->reseau.deconnection = true; // On deconnecte le client
        }
        else
            ReceptionPaquet(cubulus); // Sinon on envoie le paquet
    }

    if(!cubulus->cubulus.gagne)
    {
        switch(VerifieCube(&cubulus->cubulus)) // On vérifie le cube (gain ou non)
        {
            case BLANC:

                    if(!cubulus->cubulus.joueurs[2].joueur) // Si le joueur blanc n'est pas humain
                    {
                        FinJeu(cubulus,false,BLANC); //On affiche la page de fin de jeu comme perdant

                        cubulus->cubulus.gagne = true; // On enregistre le cubulus comme gagnant
                    }
                    else
                    {
                        FinJeu(cubulus,true,BLANC); // Si on joue à 3 et que le blanc gagne alors on affiche le blanc gagnant

                        cubulus->cubulus.gagne = true;
                    }


                break;
            case ROUGE:

                    if(cubulus->enReseau && (strcmp(cubulus->reseau.adresse, ADRESSE_VIDE) != 0))
                    {
                        FinJeu(cubulus,true,ROUGE); // Si c'est un humain on affiche comme gagnant

                        cubulus->cubulus.gagne = true;
                    }

                    if(cubulus->enReseau && (strcmp(cubulus->reseau.adresse, ADRESSE_VIDE) == 0))
                    {
                        FinJeu(cubulus,false,ROUGE); // Si c'est un humain on affiche comme gagnant

                        cubulus->cubulus.gagne = true;
                    }

                    if(!cubulus->cubulus.joueurs[1].joueur) //Si le joueur ROUGE n'est pas un joueur humain
                    {
                        FinJeu(cubulus,false,ROUGE); // On affiche la page de fin de jeu comme perdant

                        cubulus->cubulus.gagne = true;
                    }
                    else
                    {
                        FinJeu(cubulus,true,ROUGE); // Si c'est un humain on affiche comme gagnant

                        cubulus->cubulus.gagne = true;
                    }


                break;
            case NOIR:

                if(cubulus->enReseau && (strcmp(cubulus->reseau.adresse, ADRESSE_VIDE) != 0))
                    {
                        FinJeu(cubulus,false,NOIR); // Si c'est un humain on affiche comme gagnant

                        cubulus->cubulus.gagne = true;
                    }

                    if(cubulus->enReseau && (strcmp(cubulus->reseau.adresse, ADRESSE_VIDE) == 0))
                    {
                        FinJeu(cubulus,true,NOIR); // Si c'est un humain on affiche comme gagnant

                        cubulus->cubulus.gagne = true;
                    }

                FinJeu(cubulus,true,NOIR);

                cubulus->cubulus.gagne = true;
                break;
            default:
                break;
        }
    }



    SDL_Event event;

    while(SDL_PollEvent(&event) > 0)
    {

        if(cubulus->enReseau && !ATonTour(cubulus) && event.type != SDL_QUIT && event.key.keysym.sym != SDLK_ESCAPE)
        {
            boutonRight = false;
            boutonLeft = false;
            boutonUp = false;
            boutonDown = false;
            boutonShift = false;
            mouvementSouris = false;
            clique = false;
            continue;
        }

        switch(event.type)
        {
			case SDL_QUIT:
                retour = 1;
                    break;

            case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					retour = 1;
					break;

                case SDLK_LSHIFT:
					boutonShift = true;
					break;

				case SDLK_RIGHT:
                    if(cubulus->angleXCamera == 0 && cubulus->angleZCamera == 0 && cubulus->angleYCamera == 0 && !boutonRight && !boutonShift)
                    {
                        RotationCube(&cubulus->cubulus, DROITE);
                        cubulus->angleZCamera+=90;
                        if(cubulus->enReseau)
                            EnvoiPaquet(cubulus, ROTATION, DROITE, 0, 0);
                    }
					boutonRight = true;
					break;

				case SDLK_UP:
                    if(cubulus->angleXCamera == 0 && cubulus->angleZCamera == 0 && cubulus->angleYCamera == 0 && !boutonUp && !boutonShift)
                    {
                        RotationCube(&cubulus->cubulus, HAUT);
                        cubulus->angleYCamera-=90;
                        if(cubulus->enReseau)
                            EnvoiPaquet(cubulus, ROTATION, HAUT, 0, 0);
                    }
					boutonUp = true;
					break;

				case SDLK_DOWN:
                    if(cubulus->angleXCamera == 0 && cubulus->angleZCamera == 0 && cubulus->angleYCamera == 0 && !boutonDown && !boutonShift)
                    {
                        RotationCube(&cubulus->cubulus, BAS);
                        cubulus->angleYCamera+=90;
                        if(cubulus->enReseau)
                            EnvoiPaquet(cubulus, ROTATION, BAS, 0, 0);
                    }
					boutonDown = true;
					break;

				case SDLK_LEFT:
                    if(cubulus->angleXCamera == 0 && cubulus->angleZCamera == 0 && cubulus->angleYCamera == 0 && !boutonLeft && !boutonShift)
                    {
                        RotationCube(&cubulus->cubulus, GAUCHE);
                        cubulus->angleZCamera-=90;
                        if(cubulus->enReseau)
                            EnvoiPaquet(cubulus, ROTATION, GAUCHE, 0, 0);
                    }
					boutonLeft= true;
					break;
                 default:
                    if(cubulus->cubulus.gagne)
                        retour = 1;
				}
				break;

			case SDL_KEYUP:
				switch (event.key.keysym.sym)
				{
				case SDLK_RIGHT:
					boutonRight = false;
					break;

                case SDLK_LSHIFT:
					boutonShift = false;
					break;

				case SDLK_UP:
					boutonUp = false;
					break;

				case SDLK_DOWN:
					boutonDown = false;
					break;

				case SDLK_LEFT:
					boutonLeft= false;
					break;

				}
				break;
            case SDL_MOUSEBUTTONDOWN:
                coordClic[0] = event.motion.x;
                coordClic[1] = event.motion.y;
                clique = true;

                break;
            case SDL_MOUSEBUTTONUP:
                if(abs(coordClic[0]-event.motion.x) < 5 && abs(coordClic[1]-event.motion.y) < 5 &&
                   cubulus->angleXCamera % 90 == 0 && cubulus->angleYCamera % 90 == 0 && cubulus->angleZCamera % 90 == 0)
                {
                    GestionClique(cubulus, event.motion.x, event.motion.y);
                }
                mouvementSouris = false;
                clique = false;

                break;
            case SDL_MOUSEMOTION:
                if(abs(coordClic[0]-event.motion.x) > 5 && abs(coordClic[1]-event.motion.y) > 5 && clique)
                    mouvementSouris = true;
                if(mouvementSouris)
                {
                    cubulus->angleZCamera += event.motion.xrel;
                    cubulus->angleYCamera += event.motion.yrel;
                }
                break;


			}
    }

    if(boutonShift)
    {
        if ( boutonRight )
            cubulus->angleZCamera += PAS;

        if ( boutonUp )
            cubulus->angleYCamera -= PAS;

        if ( boutonLeft )
            cubulus->angleZCamera -= PAS;

        if ( boutonDown)
            cubulus->angleYCamera += PAS;

    }
    if(!mouvementSouris && !cubulus->opencv.detection && !boutonShift)
    {
        //Permet d'avoir des angles entre 0° et 360° (exclu)
        int* tabAngle[] = {&cubulus->angleXCamera, &cubulus->angleYCamera, &cubulus->angleZCamera};
        for( i = 0 ; i < sizeof(tabAngle)/sizeof(int*) ; i++)
        {
            if(*tabAngle[i] > 0 && *tabAngle[i] <= 180)
                (*tabAngle[i])--;
            if(*tabAngle[i] < 360 && *tabAngle[i] > 180)
                (*tabAngle[i])++;

            if(*tabAngle[i] >= 360)
                *tabAngle[i] -= 360;
            if(*tabAngle[i] < 0)
                *tabAngle[i] += 360;
        }
    }

    if(retour != 0 && cubulus->enReseau)
        cubulus->reseau.deconnection = true;

    for(i = 0 ; i < 3 ; i++)
        if(cubulus->cubulus.joueurs[i].aTonTour && !cubulus->cubulus.joueurs[i].joueur && !cubulus->cubulus.gagne)
        {
            enum Face face = IAJoue(&cubulus->cubulus);
            switch(face)
            {
            case F_DROITE:
                cubulus->angleZCamera+=90;
                break;
            case F_ARRIERE:
                cubulus->angleZCamera+=180;
                break;
            case F_GAUCHE:
                cubulus->angleZCamera-=90;
                break;
            case F_DESSUS:
                cubulus->angleYCamera-=90;
                break;
            case F_DESSOUS:
                cubulus->angleYCamera+=90;
                break;
            default:
                break;
            }
        }


    return retour;
}

void GestionClique(CubulusContext *cubulus, int x, int y)
{
    int posX = x / 125;
    int posY = y / 125;
    int face = 1;
    int i;



    if(posX > 0 && posX <= TAILLE_CUBE)
        if(posY > 0 && posY <= TAILLE_CUBE)
        {
            if(Jouer(&cubulus->cubulus, posX, posY))
            {

                if(cubulus->enReseau)
                    EnvoiPaquet(cubulus, JOUER, posX, posY, 0);

                Draw(cubulus);
                for(i = 0 ; i < 3 ; i++)
                    while(cubulus->cubulus.joueurs[i].aTonTour && !cubulus->cubulus.joueurs[i].joueur && !cubulus->cubulus.gagne)
                    {
                        face = IAJoue(&cubulus->cubulus);
                        switch(face)
                        {
                        case F_DROITE:
                            cubulus->angleZCamera+=90;
                            break;
                        case F_ARRIERE:
                            cubulus->angleZCamera+=180;
                            break;
                        case F_GAUCHE:
                            cubulus->angleZCamera-=90;
                            break;
                        case F_DESSUS:
                            cubulus->angleYCamera-=90;
                            break;
                        case F_DESSOUS:
                            cubulus->angleYCamera+=90;
                            break;
                        default:
                            break;
                        }
                    }
            }
        }
}

int Initialisation(CubulusContext* cubulus)
{
    if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO) < 0)
    {
        printf("Erreur lors de l'initialisation de la SDL\n%s\n", SDL_GetError());
        SDL_Quit();
        return -1;
    }
    if(TTF_Init() < 0)
    {
        printf("TTF_Init: %s\n", TTF_GetError());
        return -1;
    }

    if(Mix_Init(MIX_INIT_OGG) < 0)
    {
        printf("Mix_Init: %s\n", Mix_GetError());
        return -1;
    }
    if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024)!=0)
    {
        printf("Mix_OpenAudio: %s\n", Mix_GetError());
        return -1;
    }

    srand(time(NULL));

    cubulus->musiqueFond = Mix_LoadMUS("./data/Outlets of the Sky.ogg");
    if(cubulus->musiqueFond == NULL)
        return -1;
    Mix_VolumeMusic(MIX_MAX_VOLUME/5);

    cubulus->police = TTF_OpenFont(POLICE, 30);
    if(cubulus->police == NULL)
    {
        printf("TTF_OpenFont: %s\n", TTF_GetError());
        return -1;
    }
    int argc = 1;
    char *argv[1] = {(char*)"Something"};
    glutInit(&argc,argv);

   	cubulus->fenetre = SDL_CreateWindow("Cubulus by Jubirek ©", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, LARGEUR_FENETRE, HAUTEUR_FENETRE, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);

   	cubulus->contexteOpenGL = SDL_GL_CreateContext(cubulus->fenetre);

   	cubulus->rendu = SDL_CreateRenderer(cubulus->fenetre,-1,SDL_RENDERER_ACCELERATED);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(70, (double)1,1,1000);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_BLEND) ;
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA) ;

	cubulus->angleZCamera = 0;
	cubulus->angleYCamera = 0;
	cubulus->angleXCamera = 0;

	cubulus->enReseau = false;


	VideCube(&cubulus->cubulus);

	cubulus->opencv.thread = NULL;
	cubulus->opencv.detection = false;
	cubulus->opencv.stopThread = false;

	cubulus->opencv.capture = NULL;

	Mix_PlayMusic(cubulus->musiqueFond, -1);

	return 0;
}


void DestroyCubulus(CubulusContext* cubulus)
{
    StopKinect(cubulus);

    TTF_CloseFont(cubulus->police);
    SDL_GL_DeleteContext(cubulus->contexteOpenGL);
    SDL_DestroyWindow(cubulus->fenetre);

    while(!Mix_FadeOutMusic(2000) && Mix_PlayingMusic())
    {
        SDL_Delay(100);
    }
    if(cubulus->musiqueFond != NULL)
    Mix_FreeMusic(cubulus->musiqueFond);
    Mix_CloseAudio();
    Mix_Quit();

    SDL_Quit();
}


void Regles(CubulusContext* cubulus)
{
	bool continuer = true, retour = false;

	Surface surface[19];

	SDL_Color rouge = {255, 0, 0};
	SDL_Color noir = {0, 0, 0};

    SDL_Event event;

	TTF_Font *policeGros = TTF_OpenFont(POLICETITRE, 90);
	TTF_Font *police = TTF_OpenFont(POLICE, 17);
    TTF_Font *policePetite = TTF_OpenFont(POLICE, 15);

    surface[0].surface = IMG_Load("./data/fond.png");
    surface[1].surface = IMG_Load("./data/Cubulus.png");
	surface[2].surface = IMG_Load("./data/combinaison1.png");
    surface[3].surface = IMG_Load("./data/combinaison2.png");
    surface[4].surface = IMG_Load("./data/combinaison3.png");
    surface[17].surface = IMG_Load("./data/retour.png");
    surface[18].surface = IMG_Load("./data/retourHover.png");

    surface[5].surface = TTF_RenderUTF8_Blended(policeGros, "Règles", noir);
    surface[6].surface = TTF_RenderText_Blended(police, "Objectif : ", rouge);
    surface[7].surface = TTF_RenderUTF8_Blended(police, "Soyez le premier à former un carré de votre couleur !", noir);
    surface[8].surface = TTF_RenderText_Blended(policePetite, "> Manipulez le cube et observez-le bien sous tous les angles...", noir);
    surface[9].surface = TTF_RenderUTF8_Blended(policePetite, "> Placez-y une boule ou modifiez l'ordre d'une ligne pleine...", noir);
    surface[10].surface = TTF_RenderText_Blended(policePetite, "> Passez le cube au joueur suivant. ", noir);
    surface[11].surface = TTF_RenderText_Blended(policePetite, "Le cube se remplit et le jeu se corse : ", noir);
    surface[12].surface = TTF_RenderUTF8_Blended(policePetite, "Prenez garde à ne pas favoriser l'adversaire!", noir);
    surface[13].surface = TTF_RenderText_Blended(police, "2 joueurs : ", rouge);
    surface[14].surface = TTF_RenderUTF8_Blended(police, "Définissez une couleur neutre", noir);
    surface[15].surface = TTF_RenderText_Blended(police, "3 joueurs : ", rouge);
    surface[16].surface = TTF_RenderText_Blended(police, "Choisissez votre couleur", noir);


    surface[0].position.x = 0;
    surface[0].position.y = 0;

    surface[5].position.x = (LARGEUR_FENETRE/2) - (surface[5].surface->w/2);
    surface[5].position.y = 67;

    surface[6].position.x = 5;
    surface[6].position.y = 200;

    surface[7].position.x = surface[6].surface->w + 8;
    surface[7].position.y = 200;

    surface[8].position.x = (LARGEUR_FENETRE/2) - (surface[8].surface->w/2);
    surface[8].position.y = 320;

    surface[9].position.x = (LARGEUR_FENETRE/2) - (surface[8].surface->w/2);
    surface[9].position.y = 360;

    surface[10].position.x = (LARGEUR_FENETRE/2) - (surface[8].surface->w/2);
    surface[10].position.y = 400;

    surface[11].position.x = (LARGEUR_FENETRE/2) - (surface[11].surface->w/2);
    surface[11].position.y = 440;

    surface[12].position.x = (LARGEUR_FENETRE/2) - (surface[12].surface->w/2);
    surface[12].position.y = 480;

    surface[13].position.x = 5;
    surface[13].position.y = 520;

    surface[14].position.x = surface[13].surface->w + 8;
    surface[14].position.y = 520;

    surface[15].position.x = 5;
    surface[15].position.y = 550;

    surface[16].position.x = surface[15].surface->w + 8;
    surface[16].position.y = 550;

    surface[17].position.x = 5;
    surface[17].position.y = 5;

    surface[18].position.x = 5;
    surface[18].position.y = 5;

    surface[1].position.x = LARGEUR_FENETRE-125;
	surface[1].position.y = 475;

	surface[2].position.x = 130;
	surface[2].position.y = 230;

	surface[3].position.x = 250;
	surface[3].position.y = 230;

	surface[4].position.x = 370;
	surface[4].position.y = 230;

    while(continuer)
    {
        while(SDL_PollEvent(&event) > 0)
        {
            switch(event.type)
            {
			case SDL_QUIT:
                continuer = false;
                break;
            case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					continuer = false;
					break;
				}
			case SDL_MOUSEMOTION:

				if((event.motion.x >= surface[17].position.x) && (event.motion.x <= surface[17].position.x + (surface[17].surface->w))
					&& (event.motion.y >= surface[17].position.y) && (event.motion.y <= surface[17].position.y + (surface[17].surface->h)) )
					{
						if(!retour)
						{
							retour = !retour;
						}
					}
				else if(retour)
					{
						retour = !retour;
					}
			break;

			case SDL_MOUSEBUTTONDOWN:

				if((event.motion.x >= surface[17].position.x) && (event.motion.x <= surface[17].position.x + (surface[17].surface->w))
					&& (event.motion.y >= surface[17].position.y) && (event.motion.y <= surface[17].position.y + (surface[17].surface->h)) )
					{
						continuer = false;
					}
				break;
			}
		}

		for(int i = 0 ; i < 18 ; i++)
			SDL_BlitSurface(surface[i].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[i].position);

		if(retour)
			SDL_BlitSurface(surface[18].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[18].position);

        SDL_UpdateWindowSurface(cubulus->fenetre);
        SDL_Delay(10);
    }

	for(int i = 0 ; i < 19 ; i++)
			SDL_FreeSurface(surface[i].surface);


    TTF_CloseFont(police);
    TTF_CloseFont(policeGros);
    TTF_CloseFont(policePetite);

}

void Credits(CubulusContext* cubulus)
{
	bool continuer = true, retour = false;

	Surface surface[11];

	SDL_Color rouge = {255, 0, 0};
	SDL_Color noir = {0, 0, 0};

    SDL_Event event;

	TTF_Font *policeGros = TTF_OpenFont(POLICETITRE, 90);
	TTF_Font *police = TTF_OpenFont(POLICE, 17);
    TTF_Font *policePetite = TTF_OpenFont(POLICE, 15);

    surface[0].surface = NULL;
    surface[1].surface = NULL;
    surface[2].surface = NULL;
    surface[3].surface = TTF_RenderUTF8_Blended(policeGros, "Crédits", noir);
    surface[4].surface = TTF_RenderUTF8_Blended(policePetite, "Cubulus est un jeu développé dans le cadre d'un projet informatique.", noir);
    surface[5].surface = TTF_RenderUTF8_Blended(policePetite, "L'équipe de développement Jubirek est composée de : ", noir);
    surface[6].surface = TTF_RenderUTF8_Blended(police, "Tarek BOUAZIZ, Julien CASSAGNE et Fabien MARTINET", rouge);
    surface[7].surface = TTF_RenderUTF8_Blended(policePetite, "Remerciements à Mr.COLONNA, Mr.DELAFOSSE et Mr.OLIVI", noir);
    surface[8].surface = TTF_RenderUTF8_Blended(police, "...Passez un bon moment avec le CUBULUS !", rouge);
    surface[9].surface = NULL;
    surface[10].surface = NULL;

    surface[0].position.x = 0;
    surface[0].position.y = 0;

    surface[3].position.x = (LARGEUR_FENETRE/2) - (surface[5].surface->w/2);
    surface[3].position.y = 67;

    surface[4].position.x = 15;
    surface[4].position.y = 250;

    surface[5].position.x = 15;
    surface[5].position.y = 300;

    surface[6].position.x = (LARGEUR_FENETRE/2) - (surface[6].surface->w/2);
    surface[6].position.y = 370;

    surface[7].position.x = (LARGEUR_FENETRE/2) - (surface[7].surface->w/2);
    surface[7].position.y = 440;

    surface[8].position.x = 70;
    surface[8].position.y = 525;

    surface[9].position.x = 5;
    surface[9].position.y = 5;

    surface[1].position.x = LARGEUR_FENETRE-125;
	surface[1].position.y = 475;

	surface[2].position.x = (LARGEUR_FENETRE/2) - 80;
	surface[2].position.y = 175;


	surface[0].surface = IMG_Load("./data/fond.png");
    surface[1].surface = IMG_Load("./data/Cubulus.png");
    surface[2].surface = IMG_Load("./data/logoisen.png");
    surface[9].surface = IMG_Load("./data/retour.png");
    surface[10].surface = IMG_Load("./data/retourHover.png");

    while(continuer)
    {
        while(SDL_PollEvent(&event) > 0)
        {
            switch(event.type)
            {
			case SDL_QUIT:
                continuer = false;
                break;
            case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					continuer = false;
					break;
				}
			case SDL_MOUSEMOTION:

				if((event.motion.x >= surface[9].position.x) && (event.motion.x <= surface[9].position.x + (surface[9].surface->w))
					&& (event.motion.y >= surface[9].position.y) && (event.motion.y <= surface[9].position.y + (surface[9].surface->h)) )
					{
						if(!retour)
						{
							retour = !retour;
						}
					}
				else if(retour)
					{
						retour = !retour;
					}
			break;

			case SDL_MOUSEBUTTONDOWN:

				if((event.motion.x >= surface[9].position.x) && (event.motion.x <= surface[9].position.x + (surface[9].surface->w))
					&& (event.motion.y >= surface[9].position.y) && (event.motion.y <= surface[9].position.y + 30) )
					{
						continuer = false;
					}
				break;
			}
		}

		for(int i = 0 ; i < 10 ; i++)
			SDL_BlitSurface(surface[i].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[i].position);

		if(retour)
			SDL_BlitSurface(surface[10].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[9].position);

        SDL_UpdateWindowSurface(cubulus->fenetre);
        SDL_Delay(10);
    }

	for(int i = 0 ; i < 11 ; i++)
			SDL_FreeSurface(surface[i].surface);


    TTF_CloseFont(police);
    TTF_CloseFont(policeGros);
    TTF_CloseFont(policePetite);

}

bool FinJeu(CubulusContext* cubulus, bool victoire, enum Couleur couleur)
{

	bool quitter = false, motionQuitter = false;

	Surface surface[9];

	SDL_Color rouge = {255, 0, 0};
	SDL_Color noir = {0, 0, 0};

    SDL_Event event;

	TTF_Font *policeGros = TTF_OpenFont(POLICETITRE, 90);
	TTF_Font *police = TTF_OpenFont(POLICE, 30);
    TTF_Font *policePetite = TTF_OpenFont(POLICE, 15);

    surface[0].surface = IMG_Load("./data/fond.png");
    surface[1].surface = IMG_Load("./data/noir.png");
    surface[2].surface = IMG_Load("./data/rouge.png");
    surface[3].surface = TTF_RenderUTF8_Blended(policeGros, "VICTOIRE !", rouge);
    surface[4].surface = TTF_RenderUTF8_Blended(policeGros, "PERDU !", rouge);
    surface[5].surface = TTF_RenderUTF8_Blended(police, "Vous avez battu votre adversaire !", noir);
    surface[6].surface = TTF_RenderUTF8_Blended(police, "Vous avez été battu !", noir);
    surface[7].surface = TTF_RenderUTF8_Blended(police, "Continuer", noir);
    surface[8].surface = IMG_Load("./data/blanc.png");

    surface[0].position.x = 0;
    surface[0].position.y = 0;

    surface[1].position.x = (LARGEUR_FENETRE/2) - (surface[1].surface->w/2);
    surface[1].position.y = (HAUTEUR_FENETRE/2) - (surface[1].surface->h/2);

    surface[2].position.x = (LARGEUR_FENETRE/2) - (surface[2].surface->w/2);
    surface[2].position.y = (HAUTEUR_FENETRE/2) - (surface[2].surface->h/2);

    surface[8].position.x = (LARGEUR_FENETRE/2) - (surface[8].surface->w/2);
    surface[8].position.y = (HAUTEUR_FENETRE/2) - (surface[8].surface->h/2);

    surface[3].position.x = (LARGEUR_FENETRE/2) - (surface[3].surface->w/2);
    surface[3].position.y = 67;

    surface[4].position.x = (LARGEUR_FENETRE/2) - (surface[4].surface->w/2);
    surface[4].position.y = 67;

    surface[5].position.x = (LARGEUR_FENETRE/2) - (surface[5].surface->w/2);
    surface[5].position.y = 180;

    surface[6].position.x = (LARGEUR_FENETRE/2) - (surface[6].surface->w/2);
    surface[6].position.y = 180;

    surface[7].position.x = (LARGEUR_FENETRE/2) - (surface[7].surface->w/2);
	surface[7].position.y = 410;

    while(!quitter)
    {
        while(SDL_PollEvent(&event) > 0)
        {
            switch(event.type)
            {
			case SDL_QUIT:
                quitter = true;
                break;
            case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					quitter = true;
					break;
				}
			case SDL_MOUSEMOTION:

				if((event.motion.x >= surface[7].position.x) && (event.motion.x <= surface[7].position.x + (surface[7].surface->w))
					&& (event.motion.y >= surface[7].position.y) && (event.motion.y <= surface[7].position.y + (surface[7].surface->h)) )
					{
						if(!motionQuitter)
						{
                            ChangerCouleur(NOIR, &surface[7].surface, "Continuer", TTF_OpenFont(POLICE, 30));
							motionQuitter = !motionQuitter;
						}
					}
				else if(motionQuitter)
					{
                        ChangerCouleur(ROUGE, &surface[7].surface, "Continuer", TTF_OpenFont(POLICE, 30));
						motionQuitter = !motionQuitter;
					}
			break;

			case SDL_MOUSEBUTTONDOWN:

                if((event.motion.x >= surface[7].position.x) && (event.motion.x <= surface[7].position.x + (surface[7].surface->w))
					&& (event.motion.y >= surface[7].position.y) && (event.motion.y <= surface[7].position.y + (surface[7].surface->h)) )
					{
						quitter = true;
					}
				break;
			}
		}

		SDL_BlitSurface(surface[0].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[0].position);
		SDL_BlitSurface(surface[7].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[7].position);

		if(victoire)
		{
            for(int i=3;i<6;i+=2)
                SDL_BlitSurface(surface[i].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[i].position);
            if(couleur==ROUGE)
                SDL_BlitSurface(surface[2].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[2].position);
            if(couleur==NOIR)
                SDL_BlitSurface(surface[1].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[1].position);
            if(couleur==BLANC)
                SDL_BlitSurface(surface[8].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[8].position);
		}

		if(!victoire)
		{
            for(int i=4;i<7;i+=2)
                SDL_BlitSurface(surface[i].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[i].position);
            if(couleur==ROUGE)
                SDL_BlitSurface(surface[2].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[2].position);
            if(couleur==NOIR)
                SDL_BlitSurface(surface[1].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[1].position);
            if(couleur==BLANC)
                SDL_BlitSurface(surface[8].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[8].position);
		}

        SDL_UpdateWindowSurface(cubulus->fenetre);
        SDL_Delay(10);
    }

	for(int i = 0 ; i < 9 ; i++)
			SDL_FreeSurface(surface[i].surface);


    TTF_CloseFont(police);
    TTF_CloseFont(policeGros);
    TTF_CloseFont(policePetite);

    return quitter;

}

enum Couleur Tirage(CubulusContext* cubulus)
{

	bool clic = false;
	cubulus->cubulus.couleurDebut = VOID;

	int angle1=1, angle2=0, angle3=0;

	enum Couleur couleur;

	Surface surface[6];

	SDL_Color noir = {0, 0, 0};

    SDL_Event event;

	TTF_Font *policeGros = TTF_OpenFont(POLICETITRE, 90);
	TTF_Font *police = TTF_OpenFont(POLICE, 20);

    surface[0].surface = IMG_Load("./data/fond.png");
    surface[5].surface = IMG_Load("./data/rouge.png");
    surface[4].surface = IMG_Load("./data/noir.png");
    surface[3].surface = IMG_Load("./data/blanc.png");
    SDL_Surface *rougeRotation = NULL;
    SDL_Surface *noirRotation = NULL;
    SDL_Surface *blancRotation = NULL;

    surface[1].surface = TTF_RenderUTF8_Blended(policeGros, "Tirage", noir);
    surface[2].surface = TTF_RenderUTF8_Blended(police, "Cliquez pour tirer au sort un joueur", noir);


    surface[0].position.x = 0;
    surface[0].position.y = 0;

    surface[1].position.x = (LARGEUR_FENETRE/2) - (surface[1].surface->w/2);
    surface[1].position.y = 67;

    surface[2].position.x = (LARGEUR_FENETRE/2) - (surface[2].surface->w/2);
    surface[2].position.y = 200;

    surface[5].position.x = 150;
    surface[5].position.y = (HAUTEUR_FENETRE/2) - (surface[5].surface->h/2);

    surface[4].position.x = 300;
    surface[4].position.y = (HAUTEUR_FENETRE/2) - (surface[4].surface->h/2);

    surface[3].position.x = (LARGEUR_FENETRE/2) - (surface[3].surface->w/2);
    surface[3].position.y = (HAUTEUR_FENETRE/2) - (surface[3].surface->h/2) + 198;

    while(cubulus->cubulus.couleurDebut == VOID)
    {
        while(SDL_PollEvent(&event) > 0)
        {
            if((cubulus->enReseau && (strcmp(cubulus->reseau.adresse, ADRESSE_VIDE) == 0)) || (!cubulus->enReseau))
            {
                switch(event.type)
                {

                case SDL_MOUSEBUTTONDOWN:

                    if(!clic)
                    {
                        if((event.motion.x >= surface[0].position.x) && (event.motion.x <= surface[0].position.x + (surface[0].surface->w))
                            && (event.motion.y >= surface[0].position.y) && (event.motion.y <= surface[0].position.y + (surface[0].surface->h)) )
                            {
                                    clic = !clic;
                                    couleur = TireAuSort(&cubulus->cubulus);
                                    cubulus->cubulus.couleurDebut = couleur;
                                    if(cubulus->enReseau)
                                        EnvoiPaquet(cubulus, TIRAGE, couleur, 0, 0);

                            }
                    }
                    break;
                }
			}
		}
		if(cubulus->enReseau && (strcmp(cubulus->reseau.adresse, ADRESSE_VIDE) != 0))
            ReceptionPaquet(cubulus);

		SDL_BlitSurface(surface[0].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[0].position);
		SDL_BlitSurface(surface[1].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[1].position);
		SDL_BlitSurface(surface[2].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[2].position);

        if(cubulus->cubulus.couleurDebut == VOID)
        {
            rougeRotation = rotozoomSurface (surface[5].surface, ++angle1, 1,1);
            noirRotation = rotozoomSurface (surface[4].surface, angle2-=1.5, 1,1);
            blancRotation = rotozoomSurface (surface[3].surface, angle3-=1.25,1,1);

            SDL_BlitSurface(rougeRotation, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[5].position);
            SDL_BlitSurface(noirRotation, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[4].position);
            if(cubulus->cubulus.troisJoueur)
                SDL_BlitSurface(blancRotation, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[3].position);

            SDL_UpdateWindowSurface(cubulus->fenetre);
            SDL_FreeSurface(rougeRotation);
            SDL_FreeSurface(noirRotation);
            SDL_FreeSurface(blancRotation);

            if(angle1>= 360)
                angle1 -=360;
            if(angle2>= 360)
                angle2 -=360;
            if(angle3>= 360)
                angle3 -=360;

            if(angle1< 0)
                angle1 += 360;
            if(angle2< 0)
                angle2 += 360;
            if(angle3< 0)
                angle3 += 360;
        }

        if(cubulus->cubulus.couleurDebut != VOID)
        {
            surface[3].position.x = (LARGEUR_FENETRE/2) - (surface[3].surface->w/2);
            surface[3].position.y = (HAUTEUR_FENETRE/2) - (surface[3].surface->h/2);
            surface[4].position.x = (LARGEUR_FENETRE/2) - (surface[4].surface->w/2);
            surface[4].position.y = (HAUTEUR_FENETRE/2) - (surface[4].surface->h/2);
            surface[5].position.x = (LARGEUR_FENETRE/2) - (surface[5].surface->w/2);
            surface[5].position.y = (HAUTEUR_FENETRE/2) - (surface[5].surface->h/2);

            couleur = cubulus->cubulus.couleurDebut;
            SDL_BlitSurface(surface[cubulus->cubulus.couleurDebut+3].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[cubulus->cubulus.couleurDebut+3].position);
        }

        SDL_UpdateWindowSurface(cubulus->fenetre);
        if(cubulus->cubulus.couleurDebut != VOID)
            SDL_Delay(2000);
        else
            SDL_Delay(10);

    }

	for(int i = 0 ; i < 6 ; i++)
			SDL_FreeSurface(surface[i].surface);


    TTF_CloseFont(police);
    TTF_CloseFont(policeGros);

    return couleur;

}

bool Hebergement(CubulusContext* cubulus)
{
    bool quitter = false, retour = false;

    int angle = 0;

	Surface surface[6];
	SDL_Surface *cubeRotation = NULL;

	SDL_Color noir = {0, 0, 0};

    SDL_Event event;

	TTF_Font *police = TTF_OpenFont(POLICE, 30);
	TTF_Font *policeGros = TTF_OpenFont(POLICETITRE, 50);

    surface[0].surface = IMG_Load("./data/fond.png");
    surface[4].surface = IMG_Load("./data/Cubulus.png");
    surface[3].surface = IMG_Load("./data/retour.png");
    surface[5].surface = IMG_Load("./data/retourHover.png");

    surface[1].surface = TTF_RenderUTF8_Blended(police, "En attente d'adversaire...", noir);
    surface[2].surface = TTF_RenderUTF8_Blended(policeGros, "Hebergement", noir);


    surface[0].position.x = 0;
    surface[0].position.y = 0;

    surface[1].position.x = (LARGEUR_FENETRE/2) - (surface[1].surface->w/2);
    surface[1].position.y = (HAUTEUR_FENETRE/2) - (surface[1].surface->h/2);

    surface[2].position.x = (LARGEUR_FENETRE/2) - (surface[2].surface->w/2);
    surface[2].position.y = 107;

    surface[4].position.x = (LARGEUR_FENETRE/2) - 100;
    surface[4].position.y = (HAUTEUR_FENETRE/2) - (surface[3].surface->h/2) +100;

    surface[3].position.x = 5;
    surface[3].position.y = 5;

    while(!quitter && !cubulus->reseau.connecte && cubulus->reseau.erreur == 0)
    {
        while(SDL_PollEvent(&event) > 0)
        {
            switch(event.type)
            {
			case SDL_QUIT:
                quitter = true;
                break;
            case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					quitter = true;
					break;
				}
			break;

            case SDL_MOUSEMOTION:

				if((event.motion.x >= surface[3].position.x) && (event.motion.x <= surface[3].position.x + (surface[3].surface->w))
					&& (event.motion.y >= surface[3].position.y) && (event.motion.y <= surface[3].position.y + (surface[3].surface->h)) )
					{
						if(!retour)
						{
							retour = !retour;
						}
					}
				else if(retour)
					{
						retour = !retour;
					}
			break;

			case SDL_MOUSEBUTTONDOWN:

				if((event.motion.x >= surface[3].position.x) && (event.motion.x <= surface[3].position.x + (surface[3].surface->w))
					&& (event.motion.y >= surface[3].position.y) && (event.motion.y <= surface[3].position.y + (surface[3].surface->h)) )
					{
						quitter = true;
					}
				break;

            }


        }
        for(int i=0 ; i<4 ; i++)
            SDL_BlitSurface(surface[i].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[i].position);

        if(retour)
			SDL_BlitSurface(surface[5].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[3].position);

        cubeRotation = rotozoomSurface (surface[4].surface, ++angle, 1,1);
        SDL_BlitSurface(cubeRotation, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[4].position);
        SDL_UpdateWindowSurface(cubulus->fenetre);
        SDL_FreeSurface(cubeRotation);
        if(angle>= 360)
            angle -=360;

        SDL_Delay(10);

    }

    for(int i = 0 ; i < 6 ; i++)
        SDL_FreeSurface(surface[i].surface);

    TTF_CloseFont(police);
    TTF_CloseFont(policeGros);

    return quitter;
}



bool Options(CubulusContext* cubulus)
{
	int i;

	bool retour=false, continuer=true, menu=true, doubleJ = false, tripleJ = false, JvJ = false, JvI = false, JvJvI = false,
		 JvIvI=false, JvJvJ=false, kinect = false, jouer=false, IAF= false, IAM = false, IAD = false;

	Surface surface[40];

	SDL_Color noir = {0, 0, 0};

    SDL_Event event;

	TTF_Font *policeGros = TTF_OpenFont(POLICETITRE, 90);
	TTF_Font *police = TTF_OpenFont(POLICE, 17);
    TTF_Font *policePetite = TTF_OpenFont(POLICE, 15);
    TTF_Font *policeJouer = TTF_OpenFont(POLICE, 35);

    surface[0].surface = NULL;

	for(i=1;i<15;i++)
		surface[i].surface = NULL;

	surface[26].surface=NULL;
	surface[27].surface=NULL;
	surface[28].surface=NULL;
	surface[29].surface=NULL;
	surface[30].surface=NULL;
	surface[31].surface=NULL;
	surface[32].surface=NULL;
	surface[33].surface=NULL;
	surface[38].surface=NULL;
	surface[39].surface=NULL;


    surface[15].surface = TTF_RenderUTF8_Blended(policeGros, "Options", noir);
    surface[16].surface = TTF_RenderUTF8_Blended(police, "2 Joueurs", noir);
    surface[17].surface = TTF_RenderUTF8_Blended(police, "3 Joueurs", noir);
    surface[18].surface = TTF_RenderUTF8_Blended(police, "Joueur vs IA", noir);
    surface[19].surface = TTF_RenderUTF8_Blended(police, "Joueur vs Joueur", noir);
    surface[20].surface = TTF_RenderUTF8_Blended(police, "Joueur vs IA vs IA", noir);
    surface[21].surface = TTF_RenderUTF8_Blended(police, "Joueur vs Joueur vs IA", noir);
    surface[25].surface = TTF_RenderUTF8_Blended(police, "Joueur vs Joueur vs Joueur",noir);
    surface[22].surface = TTF_RenderUTF8_Blended(police, "Kinect® ", noir);
    surface[23].surface = NULL;
    surface[24].surface = TTF_RenderUTF8_Blended(policeJouer, "Affronter le cubulus",noir);
    surface[34].surface = TTF_RenderUTF8_Blended(police, "IA Facile",noir);
    surface[35].surface = TTF_RenderUTF8_Blended(police, "IA Moyen",noir);
    surface[36].surface = TTF_RenderUTF8_Blended(police, "IA Difficile",noir);
    surface[37].surface = NULL;


    surface[0].position.x = 0;
    surface[0].position.y = 0;

    surface[15].position.x = (LARGEUR_FENETRE/2) - (surface[15].surface->w/2);
    surface[15].position.y = 67;

    surface[16].position.x = 170;
    surface[16].position.y = 250;

    surface[17].position.x = 345;
    surface[17].position.y = 250;

    surface[18].position.x = (LARGEUR_FENETRE/2) - (surface[18].surface->w/2);
    surface[18].position.y = 300;

    surface[19].position.x = (LARGEUR_FENETRE/2) - (surface[18].surface->w/2);
    surface[19].position.y = 340;

    surface[20].position.x = (LARGEUR_FENETRE/2) - (surface[20].surface->w/2);
    surface[20].position.y = 300;

    surface[21].position.x = (LARGEUR_FENETRE/2) - (surface[20].surface->w/2);
    surface[21].position.y = 340;

    surface[25].position.x = (LARGEUR_FENETRE/2) - (surface[25].surface->w/2);
	surface[25].position.y = 380;

    surface[22].position.x = (LARGEUR_FENETRE/2) - (surface[22].surface->w/2);
	surface[22].position.y = 480;

	surface[34].position.x = 125;
	surface[34].position.y = 430;

	surface[35].position.x = 100 + (surface[34].surface->w) + 85;
	surface[35].position.y = 430;

	surface[36].position.x = 100 + (surface[34].surface->w) + (surface[35].surface->w) + 145;
	surface[36].position.y = 430;

	surface[1].position.x = 145;
	surface[1].position.y = 250;

	surface[2].position.x = 145;
	surface[2].position.y = 250;

	surface[3].position.x = 320;
	surface[3].position.y = 250;

	surface[4].position.x = 320;
	surface[4].position.y = 250;

	surface[5].position.x = (LARGEUR_FENETRE/2) - (surface[18].surface->w/2) - 25;
	surface[5].position.y = 300;

	surface[6].position.x = (LARGEUR_FENETRE/2) - (surface[18].surface->w/2) - 25;
	surface[6].position.y = 300;

	surface[7].position.x = (LARGEUR_FENETRE/2) - (surface[18].surface->w/2) - 25;
	surface[7].position.y = 340;

	surface[8].position.x = (LARGEUR_FENETRE/2) - (surface[18].surface->w/2) - 25;
	surface[8].position.y = 340;

	surface[9].position.x = (LARGEUR_FENETRE/2) - (surface[20].surface->w/2) - 25;
	surface[9].position.y = 300;

	surface[10].position.x = (LARGEUR_FENETRE/2) - (surface[20].surface->w/2) - 25;
	surface[10].position.y = 300;

	surface[11].position.x = (LARGEUR_FENETRE/2) - (surface[20].surface->w/2) - 25;
	surface[11].position.y = 340;

	surface[12].position.x = (LARGEUR_FENETRE/2) - (surface[20].surface->w/2) - 25;
	surface[12].position.y = 340;

	surface[13].position.x = (LARGEUR_FENETRE/2) - (surface[22].surface->w/2) - 45;
	surface[13].position.y = 480;

	surface[14].position.x = (LARGEUR_FENETRE/2) - (surface[22].surface->w/2) - 45;
	surface[14].position.y = 480;

	surface[28].position.x = 100;
	surface[28].position.y = 430;

	surface[29].position.x = 100;
	surface[29].position.y = 430;

	surface[30].position.x = 100 + (surface[34].surface->w) + 60;
	surface[30].position.y = 430;

	surface[31].position.x = 100 + (surface[34].surface->w) + 60;
	surface[31].position.y = 430;

	surface[32].position.x = 100 + (surface[34].surface->w) + (surface[35].surface->w) + 120;
	surface[32].position.y = 430;

	surface[33].position.x = 100 + (surface[34].surface->w) + (surface[35].surface->w) + 120;
	surface[33].position.y = 430;

	surface[26].position.x = (LARGEUR_FENETRE/2) - (surface[25].surface->w/2) - 25;
	surface[26].position.y = 380;

	surface[27].position.x = (LARGEUR_FENETRE/2) - (surface[25].surface->w/2) - 25;
	surface[27].position.y = 380;

	surface[23].position.x = 5;
	surface[23].position.y = 5;

	surface[24].position.x = (LARGEUR_FENETRE/2) - (surface[24].surface->w/2);
	surface[24].position.y = 530;

	surface[38].position.x = (LARGEUR_FENETRE/2) - (surface[24].surface->w/2) - 50;
	surface[38].position.y = 517;

	surface[39].position.x = (LARGEUR_FENETRE/2) - (surface[24].surface->w/2) - 50;
	surface[39].position.y = 517;

	surface[0].surface = IMG_Load("./data/fond.png");
    surface[1].surface = IMG_Load("./data/caseVide.png");
    surface[2].surface = IMG_Load("./data/caseRemplie.png");
    surface[38].surface = IMG_Load("./data/boutton.png");
    surface[39].surface = IMG_Load("./data/boutton2.png");


    for(i=3 ; i<13 ; i += 2)
		surface[i].surface = surface[1].surface;

	for(i=4 ; i<13 ; i+=2)
		surface[i].surface = surface[2].surface;

	for(i=26 ; i<34 ; i += 2)
		surface[i].surface = surface[1].surface;

	for(i=27 ; i<34 ; i+=2)
		surface[i].surface = surface[2].surface;

	surface[23].surface = IMG_Load("./data/retour.png");
	surface[37].surface = IMG_Load("./data/retourHover.png");
	surface[13].surface = IMG_Load("./data/off.png");
	surface[14].surface = IMG_Load("./data/on.png");

    while(continuer)
    {
        while(SDL_PollEvent(&event) > 0)
        {
            switch(event.type)
            {
			case SDL_QUIT:
                continuer = false;
                break;
            case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					continuer = false;
                    menu = true;
					break;
                case SDLK_RETURN:
					continuer = false;
					retour = false;
					menu = false;
					break;
				}
			case SDL_MOUSEMOTION:

				if((event.motion.x >= surface[23].position.x) && (event.motion.x <= surface[23].position.x + (surface[23].surface->w))
					&& (event.motion.y >= surface[23].position.y) && (event.motion.y <= surface[23].position.y + (surface[23].surface->h) ))
					{
						if(!retour)
						{
							retour = !retour;
						}

					}

					else if(retour)
					{
						retour = !retour;
					}


				if((event.motion.x >= surface[24].position.x - 40) && (event.motion.x <= surface[24].position.x + (surface[24].surface->w) + 40)
					&& (event.motion.y >= surface[24].position.y - 5) && (event.motion.y <= surface[24].position.y + 47) )
					{
						if((doubleJ && ((JvI && (IAF || IAM || IAD)) || (JvJ))) || (tripleJ && ((JvIvI && (IAF || IAM || IAD)) || (JvJvI && (IAF || IAM || IAD)) || JvJvJ)))
						{
							if(!jouer)
							{
								ChangerCouleur(NOIR, &surface[24].surface, "Affronter le cubulus", TTF_OpenFont(POLICE, 35));
								jouer = !jouer;
							}
						}
					}
					else if(jouer)
						{
							ChangerCouleur(ROUGE, &surface[24].surface, "Affronter le cubulus", TTF_OpenFont(POLICE, 35));
							jouer = !jouer;
						}

			break;

			case SDL_MOUSEBUTTONDOWN:

				if((event.motion.x >= surface[23].position.x) && (event.motion.x <= surface[23].position.x + (surface[23].surface->w))
					&& (event.motion.y >= surface[23].position.y) && (event.motion.y <= surface[23].position.y + 30) )
					{
						continuer = false;
						menu = true;
					}

				if((event.motion.x >= surface[24].position.x) && (event.motion.x <= surface[24].position.x + (surface[24].surface->w))
					&& (event.motion.y >= surface[24].position.y) && (event.motion.y <= surface[24].position.y + 35) )
					{
						if((doubleJ && ((JvI && (IAF || IAM || IAD)) || (JvJ))) || (tripleJ && ((JvIvI && (IAF || IAM || IAD)) || (JvJvI && (IAF || IAM || IAD)) || JvJvJ)))
						{
							continuer = false;
							menu = false;
							retour = false;
						}
					}

				if((event.motion.x >= surface[1].position.x) && (event.motion.x <= surface[1].position.x + 20)
					&& (event.motion.y >= surface[1].position.y) && (event.motion.y <= surface[1].position.y + 20) )
					{
						if(doubleJ == false)
						{
							doubleJ = true;
							tripleJ = false;
						}
						else
						{
							doubleJ = false;
							tripleJ = false;
						}
					}

				if((event.motion.x >= surface[3].position.x) && (event.motion.x <= surface[3].position.x + 20)
					&& (event.motion.y >= surface[3].position.y) && (event.motion.y <= surface[3].position.y + 20) )
					{
						if(tripleJ == false)
						{
							doubleJ = false;
							tripleJ = true;
						}
						else
						{
							doubleJ = false;
							tripleJ = false;
						}
					}

				if((event.motion.x >= surface[5].position.x) && (event.motion.x <= surface[5].position.x + 20)
					&& (event.motion.y >= surface[5].position.y) && (event.motion.y <= surface[5].position.y + 20) )
					{
						if(JvI == false)
						{
							JvI = true;
							JvJ = false;
							JvJvI = false;
							JvIvI = false;
							JvJvJ = false;
						}
						else
						{
							JvI = false;
							JvJ = false;
							JvJvI = false;
							JvIvI = false;
							JvJvJ = false;
						}
					}

				if((event.motion.x >= surface[7].position.x) && (event.motion.x <= surface[7].position.x + 20)
					&& (event.motion.y >= surface[7].position.y) && (event.motion.y <= surface[7].position.y + 20) )
					{
						if(JvJ == false)
						{
							JvI = false;
							JvJ = true;
							JvJvI = false;
							JvIvI = false;
							JvJvJ = false;
						}
						else
						{
							JvI = false;
							JvJ = false;
							JvJvI = false;
							JvIvI = false;
							JvJvJ = false;
						}
					}

				if((event.motion.x >= surface[9].position.x) && (event.motion.x <= surface[9].position.x + 20)
					&& (event.motion.y >= surface[9].position.y) && (event.motion.y <= surface[9].position.y + 20) )
					{
						if(JvIvI == false)
						{
							JvI = false;
							JvJ = false;
							JvJvI = false;
							JvIvI = true;
							JvJvJ = false;
						}
						else
						{
							JvI = false;
							JvJ = false;
							JvJvI = false;
							JvIvI = false;
							JvJvJ = false;
						}
					}

				if((event.motion.x >= surface[11].position.x) && (event.motion.x <= surface[11].position.x + 20)
					&& (event.motion.y >= surface[11].position.y) && (event.motion.y <= surface[11].position.y + 20) )
					{
						if(JvJvI == false)
						{
							JvI = false;
							JvJ = false;
							JvJvI = true;
							JvIvI = false;
							JvJvJ = false;
						}
						else
						{
							JvI = false;
							JvJ = false;
							JvJvI = false;
							JvIvI = false;
							JvJvJ = false;
						}
					}

				if((event.motion.x >= surface[26].position.x) && (event.motion.x <= surface[26].position.x + 20)
					&& (event.motion.y >= surface[26].position.y) && (event.motion.y <= surface[26].position.y + 20) )
					{
						if(JvJvJ == false)
						{
							JvI = false;
							JvJ = false;
							JvJvI = false;
							JvIvI = false;
							JvJvJ = true;
						}
						else
						{
							JvI = false;
							JvJ = false;
							JvJvI = false;
							JvIvI = false;
							JvJvJ = false;
						}
					}

				if((event.motion.x >= surface[13].position.x) && (event.motion.x <= surface[13].position.x + 40)
					&& (event.motion.y >= surface[13].position.y) && (event.motion.y <= surface[13].position.y + 20) )
					{
						kinect = !kinect;
					}

				if((event.motion.x >= surface[28].position.x) && (event.motion.x <= surface[28].position.x + 20)
					&& (event.motion.y >= surface[28].position.y) && (event.motion.y <= surface[28].position.y + 20) )
					{
						if(IAF == false)
						{
							IAF = true;
							IAM = false;
							IAD = false;
						}
						else
						{
							IAF = false;
							IAM = false;
							IAD = false;
						}
					}

				if((event.motion.x >= surface[30].position.x) && (event.motion.x <= surface[30].position.x + 20)
					&& (event.motion.y >= surface[30].position.y) && (event.motion.y <= surface[30].position.y + 20) )
					{
						if(IAM == false)
						{
							IAF = false;
							IAM = true;
							IAD = false;
						}
						else
						{
							IAF = false;
							IAM = false;
							IAD = false;
						}
					}

				if((event.motion.x >= surface[32].position.x) && (event.motion.x <= surface[32].position.x + 20)
					&& (event.motion.y >= surface[32].position.y) && (event.motion.y <= surface[32].position.y + 20) )
					{
						if(IAD == false)
						{
							IAF = false;
							IAM = false;
							IAD = true;
						}
						else
						{
							IAF = false;
							IAM = false;
							IAD = false;
						}
					}

				break;
			}
		}


		SDL_BlitSurface(surface[0].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[0].position);
		SDL_BlitSurface(surface[1].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[1].position);
		SDL_BlitSurface(surface[3].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[3].position);

		if(jouer)
			SDL_BlitSurface(surface[39].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[39].position);

		else
			SDL_BlitSurface(surface[38].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[38].position);

		for (i=22 ; i<25 ; i++)
			SDL_BlitSurface(surface[i].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[i].position);

		for (i=15 ; i <18 ; i++)
			SDL_BlitSurface(surface[i].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[i].position);

		if(retour)
			SDL_BlitSurface(surface[37].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[23].position);

		if(kinect)
			SDL_BlitSurface(surface[14].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[14].position);

		else
			SDL_BlitSurface(surface[13].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[13].position);


		if (doubleJ)
		{
			SDL_BlitSurface(surface[2].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[2].position);
			SDL_BlitSurface(surface[18].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[18].position);
			SDL_BlitSurface(surface[19].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[19].position);
			SDL_BlitSurface(surface[5].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[5].position);
			SDL_BlitSurface(surface[7].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[7].position);

			if(JvJ)
				SDL_BlitSurface(surface[8].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[8].position);

			if(JvI)
			{
				SDL_BlitSurface(surface[28].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[28].position);
				SDL_BlitSurface(surface[30].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[30].position);
				SDL_BlitSurface(surface[32].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[32].position);
				SDL_BlitSurface(surface[34].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[34].position);
				SDL_BlitSurface(surface[35].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[35].position);
				SDL_BlitSurface(surface[36].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[36].position);
				SDL_BlitSurface(surface[6].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[6].position);
				if(IAF)
					SDL_BlitSurface(surface[29].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[29].position);
				if(IAM)
					SDL_BlitSurface(surface[31].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[31].position);
				if(IAD)
					SDL_BlitSurface(surface[33].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[33].position);
			}

		}

		if (tripleJ)
		{
			SDL_BlitSurface(surface[4].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[4].position);
			SDL_BlitSurface(surface[20].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[20].position);
			SDL_BlitSurface(surface[21].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[21].position);
			SDL_BlitSurface(surface[9].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[9].position);
			SDL_BlitSurface(surface[11].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[11].position);
			SDL_BlitSurface(surface[25].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[25].position);
			SDL_BlitSurface(surface[26].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[26].position);

			if(JvIvI)
			{
				SDL_BlitSurface(surface[28].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[28].position);
				SDL_BlitSurface(surface[30].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[30].position);
				SDL_BlitSurface(surface[32].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[32].position);
				SDL_BlitSurface(surface[34].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[34].position);
				SDL_BlitSurface(surface[35].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[35].position);
				SDL_BlitSurface(surface[36].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[36].position);
				SDL_BlitSurface(surface[10].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[10].position);
				if(IAF)
					SDL_BlitSurface(surface[29].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[29].position);
				if(IAM)
					SDL_BlitSurface(surface[31].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[31].position);
				if(IAD)
					SDL_BlitSurface(surface[33].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[33].position);
			}

			if(JvJvI)
			{
				SDL_BlitSurface(surface[28].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[28].position);
				SDL_BlitSurface(surface[30].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[30].position);
				SDL_BlitSurface(surface[32].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[32].position);
				SDL_BlitSurface(surface[34].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[34].position);
				SDL_BlitSurface(surface[35].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[35].position);
				SDL_BlitSurface(surface[36].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[36].position);
				SDL_BlitSurface(surface[12].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[12].position);

				if(IAF)
					SDL_BlitSurface(surface[29].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[29].position);
				if(IAM)
					SDL_BlitSurface(surface[31].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[31].position);
				if(IAD)
					SDL_BlitSurface(surface[33].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[33].position);
			}

			if(JvJvJ)
				SDL_BlitSurface(surface[27].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[27].position);
		}

        SDL_UpdateWindowSurface(cubulus->fenetre);
        SDL_Delay(10);
    }

	for(i = 0 ; i <3 ; i++)
		SDL_FreeSurface(surface[i].surface);
	for(i = 15 ; i < 26 ; i++)
		SDL_FreeSurface(surface[i].surface);
	for(i = 36 ; i < 40 ; i++)
		SDL_FreeSurface(surface[i].surface);


    TTF_CloseFont(police);
    TTF_CloseFont(policeGros);
    TTF_CloseFont(policePetite);

    Joueur j1, j2, j3;

    if(!retour)
    {

        if(kinect)
            LanceKinect(cubulus);
        else
            StopKinect(cubulus);

        j3.joueurReseau = false;
        j1.joueurReseau = false;
        j2.joueurReseau = false;

        if(doubleJ)
        {
            j3.couleur = VOID;
            j1.couleur = NOIR;
            j2.couleur = ROUGE;
            if(JvJ)
            {
                j1.joueur = true;
                j2.joueur = true;
            }
            else if(JvI)
            {
                j1.joueur = true;
                j2.joueur = false;
                if(IAF)
                    j2.niveauIA = 1;
                else if(IAM)
                    j2.niveauIA = 2;
                else if(IAD)
                    j2.niveauIA = 3;
            }
        }
        else if(tripleJ)
        {
            j1.couleur = NOIR;
            j2.couleur = ROUGE;
            j3.couleur = BLANC;
            if(JvJvJ)
            {
                j1.joueur = true;
                j2.joueur = true;
                j3.joueur = true;
            }
            else if(JvJvI)
            {
                j1.joueur = true;
                j2.joueur = true;
                j3.joueur = false;
                if(IAF)
                    j3.niveauIA = 1;
                else if(IAM)
                    j3.niveauIA = 2;
                else if(IAD)
                    j3.niveauIA = 3;
            }
            else if(JvIvI)
            {
                j1.joueur = true;
                j2.joueur = false;
                j3.joueur = false;
                if(IAF)
                {
                    j2.niveauIA = 1;
                    j3.niveauIA = 1;
                }
                else if(IAM)
                {
                    j2.niveauIA = 2;
                    j3.niveauIA = 2;
                }
                else if(IAD)
                {
                    j2.niveauIA = 3;
                    j3.niveauIA = 3;
                }
            }

    }

    if(!menu)
    {
        ParametrerCube(&cubulus->cubulus, j1, j2, j3);
        enum Couleur couleurJ1 = Tirage(cubulus);
        if(couleurJ1 == NOIR)
        {
            cubulus->cubulus.joueurs[0].aTonTour = true;
            cubulus->cubulus.joueurs[1].aTonTour = false;
            cubulus->cubulus.joueurs[2].aTonTour = false;
        }

        if(couleurJ1 == ROUGE)
        {
            cubulus->cubulus.joueurs[0].aTonTour = false;
            cubulus->cubulus.joueurs[1].aTonTour = true;
            cubulus->cubulus.joueurs[2].aTonTour = false;
        }

        if(couleurJ1 == BLANC)
        {
            cubulus->cubulus.joueurs[0].aTonTour = false;
            cubulus->cubulus.joueurs[1].aTonTour = false;
            cubulus->cubulus.joueurs[2].aTonTour = true;
        }
    }

    }




    return menu;

}

int Menu(CubulusContext* cubulus)
{

	bool continuer = true, jouer = false, credits = false, quitter = false, regles = false, multi =false;
	int retour = 0;

	Surface surface[9];

    SDL_Color couleur = {0, 0, 0};
    SDL_Event event;

    TTF_Font *policeGros = TTF_OpenFont(POLICETITRE, 90);
    TTF_Font *police = TTF_OpenFont(POLICE, 30);
    TTF_Font *policePetite = TTF_OpenFont(POLICE, 15);


    surface[0].surface = NULL;

    surface[1].surface = NULL;

    surface[2].surface = TTF_RenderUTF8_Blended(policeGros, "Cubulus", couleur);
    surface[3].surface = TTF_RenderUTF8_Blended(police, "Jouer", couleur);
    surface[4].surface = TTF_RenderUTF8_Blended(police, "Réseau", couleur);
    surface[5].surface = TTF_RenderUTF8_Blended(police, "Règles", couleur);
    surface[6].surface = TTF_RenderUTF8_Blended(police, "Crédits", couleur);
    surface[7].surface = TTF_RenderUTF8_Blended(police, "Quitter", couleur);
    surface[8].surface = TTF_RenderUTF8_Blended(policePetite, "Jubirek ©, 2015", couleur);

    surface[0].position.x = 0;
    surface[0].position.y = 0;

    surface[1].position.x  = (LARGEUR_FENETRE/2) - (surface[7].surface->w/2) - 20;
	surface[1].position.y  = 440;

    surface[2].position.x = (LARGEUR_FENETRE/2) - (surface[2].surface->w/2);
    surface[2].position.y = 67;

    surface[3].position.x = (LARGEUR_FENETRE/2) - (surface[3].surface->w/2);
    surface[3].position.y = 220;

    surface[4].position.x = (LARGEUR_FENETRE/2) - (surface[4].surface->w/2);
    surface[4].position.y = 260;

    surface[5].position.x = (LARGEUR_FENETRE/2) - (surface[5].surface->w/2);
    surface[5].position.y = 300;

    surface[6].position.x  = (LARGEUR_FENETRE/2) - (surface[6].surface->w/2);
    surface[6].position.y  = 340;

    surface[7].position.x  = (LARGEUR_FENETRE/2) - (surface[7].surface->w/2);
    surface[7].position.y  = 380;

    surface[8].position.x  = (LARGEUR_FENETRE/2) - (surface[7].surface->w/2);
    surface[8].position.y  = (HAUTEUR_FENETRE/2) + 282;

	surface[0].surface = IMG_Load("./data/fond.png");
    surface[1].surface = IMG_Load("./data/Cubulus.png");

    while(continuer)
    {
        while(SDL_PollEvent(&event) > 0)
        {
            switch(event.type)
            {
			case SDL_QUIT:
                continuer = false;
                retour = 1;
                break;

            case SDL_MOUSEMOTION:

				if((event.motion.x >= surface[3].position.x) && (event.motion.x <= surface[3].position.x + (surface[3].surface->w))
					&& (event.motion.y >= surface[3].position.y) && (event.motion.y <= surface[3].position.y + 30) )
					{
						if(!jouer)
						{
							ChangerCouleur(NOIR, &surface[3].surface, "Jouer", TTF_OpenFont(POLICE, 30));
							jouer = !jouer;
						}
					}
				else if(jouer)
					{
						ChangerCouleur(ROUGE, &surface[3].surface, "Jouer", TTF_OpenFont(POLICE, 30));
						jouer = !jouer;
					}


				if((event.motion.x >= surface[4].position.x) && (event.motion.x <= surface[4].position.x + (surface[4].surface->w))
					&& (event.motion.y >= surface[4].position.y) && (event.motion.y <= surface[4].position.y + 30) )
					{
						if(!multi)
						{
							ChangerCouleur(NOIR, &surface[4].surface, "Réseau", TTF_OpenFont(POLICE, 30));
							multi = !multi;
						}
					}
				else if(multi)
					{
						ChangerCouleur(ROUGE, &surface[4].surface, "Réseau", TTF_OpenFont(POLICE, 30));
						multi = !multi;
					}

				if((event.motion.x >= surface[5].position.x) && (event.motion.x <= surface[5].position.x + (surface[5].surface->w))
					&& (event.motion.y >= surface[5].position.y) && (event.motion.y <= surface[5].position.y + 30) )
					{
						if(!regles)
						{
							ChangerCouleur(NOIR, &surface[5].surface, "Règles", TTF_OpenFont(POLICE, 30));
							regles = !regles;
						}
					}
				else if(regles)
					{
						ChangerCouleur(ROUGE, &surface[5].surface, "Règles", TTF_OpenFont(POLICE, 30));
						regles = !regles;
					}

				if((event.motion.x >= surface[6].position.x) && (event.motion.x <= surface[6].position.x + (surface[6].surface->w))
					&& (event.motion.y >= surface[6].position.y) && (event.motion.y <= surface[6].position.y + 30) )
					{
						if(!credits)
						{
							ChangerCouleur(NOIR, &surface[6].surface, "Crédits", TTF_OpenFont(POLICE, 30));
							credits = !credits;
						}
					}
				else if(credits)
					{
						ChangerCouleur(ROUGE, &surface[6].surface, "Crédits", TTF_OpenFont(POLICE, 30));
						credits = !credits;
					}

				if((event.motion.x >= surface[7].position.x) && (event.motion.x <= surface[7].position.x + (surface[7].surface->w))
					&& (event.motion.y >= surface[7].position.y) && (event.motion.y <= surface[7].position.y + 30) )
					{
						if(!quitter)
						{
							ChangerCouleur(NOIR, &surface[7].surface, "Quitter", TTF_OpenFont(POLICE, 30));
							quitter = !quitter;
						}
					}
				else if(quitter)
					{
						ChangerCouleur(ROUGE, &surface[7].surface, "Quitter", TTF_OpenFont(POLICE, 30));
						quitter = !quitter;
					}

				break;

			case SDL_MOUSEBUTTONDOWN:
				if((event.motion.x >= surface[3].position.x) && (event.motion.x <= surface[3].position.x + (surface[3].surface->w))
					&& (event.motion.y >= surface[3].position.y) && (event.motion.y <= surface[3].position.y + 30) )
					{
						continuer = Options(cubulus);
						VideCube(&cubulus->cubulus);
						retour = 0;
					}

				if((event.motion.x >= surface[4].position.x) && (event.motion.x <= surface[4].position.x + (surface[4].surface->w))
					&& (event.motion.y >= surface[4].position.y) && (event.motion.y <= surface[4].position.y + 30) )
					{
						if(!Reseau(cubulus))
						{
							continuer = false;
							retour = 0;
						}

					}

				if((event.motion.x >= surface[5].position.x) && (event.motion.x <= surface[5].position.x + (surface[5].surface->w))
					&& (event.motion.y >= surface[5].position.y) && (event.motion.y <= surface[5].position.y + 30) )
					{
						Regles(cubulus);
					}

				if((event.motion.x >= surface[6].position.x) && (event.motion.x <= surface[6].position.x + (surface[6].surface->w))
					&& (event.motion.y >= surface[6].position.y) && (event.motion.y <= surface[6].position.y + 30) )
					{
						Credits(cubulus);
					}

				if((event.motion.x >= surface[7].position.x) && (event.motion.x <= surface[7].position.x + (surface[7].surface->w))
					&& (event.motion.y >= surface[7].position.y) && (event.motion.y <= surface[7].position.y + 30) )
					{
						continuer = false;
						retour = 1;
					}

            case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					continuer = false;
					retour = 1;
					break;

                case SDLK_j:
					retour= 2;
					continuer = false;
					break;
                }
                break;
            }
        }

		for(int i = 0 ; i < 9 ; i++)
			SDL_BlitSurface(surface[i].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[i].position);

        SDL_UpdateWindowSurface(cubulus->fenetre);
        SDL_Delay(10);
    }

	for(int i = 0 ; i < 9 ; i++)
		SDL_FreeSurface(surface[i].surface);

    TTF_CloseFont(police);
    TTF_CloseFont(policeGros);
    TTF_CloseFont(policePetite);

    return retour;

}

void ChangerCouleur(enum Couleur couleur, SDL_Surface **texte, char* intitule, TTF_Font *police)
{
	SDL_Color RVB;

	SDL_FreeSurface(*texte);
	if(couleur == ROUGE)
	{
		RVB.r = 0;
		RVB.g =0;
		RVB.b =0;
	}
	else
	{
		RVB.r = 255;
		RVB.g =0;
		RVB.b =0;
	}
	*texte = TTF_RenderUTF8_Blended(police, intitule, RVB);
}
void LimiteFps(int ticks)
{
    int tempsActuel;
    static int tempsPrecedent = 0;

    tempsActuel = SDL_GetTicks();
    while(SDL_GetTicks()-tempsPrecedent < ticks)
    {
        SDL_Delay(1);
    }
    tempsPrecedent = tempsActuel;
}

bool Reseau(CubulusContext* cubulus)
{
	int i;
	int compteur=0;
	InitialisationReseau(&cubulus->reseau);

	bool retour=false, continuer=true, menu =false, heberger = false, rejoindre = false, clicRejoindre =false, clicHeberger = false;

	Surface surface[8];

	SDL_Color noir = {0, 0, 0};

    SDL_Event event;

	TTF_Font *policeGros = TTF_OpenFont(POLICETITRE, 90);
	TTF_Font *police = TTF_OpenFont(POLICE, 30);
	TTF_Font *policePetite = TTF_OpenFont(POLICE, 20);

    surface[0].surface = NULL;
    surface[1].surface = NULL;
    surface[2].surface = NULL;
    surface[6].surface = NULL;
    surface[7].surface = NULL;

    surface[3].surface = TTF_RenderUTF8_Blended(policeGros, "Réseau", noir);
    surface[4].surface = TTF_RenderUTF8_Blended(police, "Héberger", noir);
    surface[5].surface = TTF_RenderUTF8_Blended(police, "Rejoindre", noir);


    surface[0].position.x = 0;
    surface[0].position.y = 0;

    surface[1].position.x = 5;
    surface[1].position.y = 5;

    surface[2].position.x = 5;
    surface[2].position.y = 5;

    surface[3].position.x = (LARGEUR_FENETRE/2) - (surface[3].surface->w/2);
    surface[3].position.y = 67;

    surface[4].position.x = (LARGEUR_FENETRE/3) - 100;
    surface[4].position.y = (HAUTEUR_FENETRE/2) - (surface[4].surface->h/2);

    surface[5].position.x = (2*LARGEUR_FENETRE/3) - (surface[5].surface->w/3);
    surface[5].position.y = (HAUTEUR_FENETRE/2) - (surface[5].surface->h/2);

    surface[6].position.x = LARGEUR_FENETRE-125;
	surface[6].position.y = 475;

	surface[7].position.x = 335;
	surface[7].position.y = (HAUTEUR_FENETRE/2) - (surface[4].surface->h/2) - 5;


	surface[0].surface = IMG_Load("./data/fond.png");
	surface[1].surface = IMG_Load("./data/retour.png");
	surface[2].surface = IMG_Load("./data/retourHover.png");
	surface[6].surface = IMG_Load("./data/Cubulus.png");
	surface[7].surface = IMG_Load("./data/caseIP.png");

    while(continuer)
    {
        while(SDL_PollEvent(&event) > 0)
        {
            switch(event.type)
            {
			case SDL_QUIT:
                continuer = false;
                menu = true;
                clicRejoindre = false;
                clicHeberger = false;
                break;
            case SDL_KEYDOWN:

				if(clicRejoindre == true)
				{
					if(event.key.keysym.sym == 8 && compteur > 0)
							cubulus->reseau.adresse[--compteur] = ' ';

					if(compteur < 16 && compteur >= 0)
					{
						if(event.key.keysym.sym==1073741923)
							cubulus->reseau.adresse[compteur++] = '.';

						if(event.key.keysym.sym==1073741922)
							cubulus->reseau.adresse[compteur++] = '0';

						if((event.key.keysym.sym) >= 1073741913 && (event.key.keysym.sym) <= 1073741921)
							cubulus->reseau.adresse[compteur++] = (event.key.keysym.sym + 48 - 1073741912);
					}
					switch (event.key.keysym.sym)
					{
						case SDLK_RETURN:
							continuer = false;
							menu = false;
						break;
					}

				}
				switch (event.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					continuer = false;
					menu = true;
					clicRejoindre = false;
                    clicHeberger = false;
					break;

				}
			case SDL_MOUSEMOTION:

				if((event.motion.x >= surface[1].position.x) && (event.motion.x <= surface[1].position.x + (surface[1].surface->w))
					&& (event.motion.y >= surface[1].position.y) && (event.motion.y <= surface[1].position.y + (surface[1].surface->h) ))
					{
						if(!retour)
						{
							retour = !retour;
						}

					}

					else if(retour)
					{
						retour = !retour;
					}

				if((clicRejoindre == false) && (event.motion.x >= surface[4].position.x) && (event.motion.x <= surface[4].position.x + (surface[4].surface->w))
					&& (event.motion.y >= surface[4].position.y) && (event.motion.y <= surface[4].position.y + (surface[4].surface->h) ))
					{
						if(!heberger)
						{
							ChangerCouleur(NOIR, &surface[4].surface, "Héberger", TTF_OpenFont(POLICE, 30));
							heberger = !heberger;
						}

					}

					else if(heberger)
					{
						ChangerCouleur(ROUGE, &surface[4].surface, "Héberger", TTF_OpenFont(POLICE, 30));
						heberger = !heberger;
					}

				if((clicRejoindre == false) && (event.motion.x >= surface[5].position.x) && (event.motion.x <= surface[5].position.x + (surface[5].surface->w))
					&& (event.motion.y >= surface[5].position.y) && (event.motion.y <= surface[5].position.y + (surface[5].surface->h) ))
					{
						if(!rejoindre)
						{
							ChangerCouleur(NOIR, &surface[5].surface, "Rejoindre", TTF_OpenFont(POLICE, 30));
							rejoindre = !rejoindre;
						}

					}

					else if(rejoindre)
					{
						ChangerCouleur(ROUGE, &surface[5].surface, "Rejoindre", TTF_OpenFont(POLICE, 30));
						rejoindre = !rejoindre;
					}

			break;

			case SDL_MOUSEBUTTONDOWN:

				if((event.motion.x >= surface[1].position.x) && (event.motion.x <= surface[1].position.x + (surface[1].surface->w))
					&& (event.motion.y >= surface[1].position.y) && (event.motion.y <= surface[1].position.y + 30) )
					{
						continuer = false;
						menu = true;
						clicRejoindre=false;
						clicHeberger=false;
					}

				if((event.motion.x >= surface[4].position.x) && (event.motion.x <= surface[4].position.x + (surface[4].surface->w))
					&& (event.motion.y >= surface[4].position.y) && (event.motion.y <= surface[4].position.y + (surface[4].surface->h)) )
					{

						heberger = true;
						clicRejoindre = false;
						clicHeberger = true;

					}

				if((event.motion.x >= surface[5].position.x) && (event.motion.x <= surface[5].position.x + (surface[5].surface->w))
					&& (event.motion.y >= surface[5].position.y) && (event.motion.y <= surface[5].position.y + (surface[5].surface->h)) )
					{

						heberger = false;
						clicRejoindre = true;
						clicHeberger = false;

					}

				break;

			}

		}

		for(i=0;i<2;i++)
			SDL_BlitSurface(surface[i].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[i].position);

		for(i=3;i<7;i++)
			SDL_BlitSurface(surface[i].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[i].position);

		if(retour)
			SDL_BlitSurface(surface[2].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[2].position);

		if(clicRejoindre)
		{
			SDL_FreeSurface(surface[4].surface);
			SDL_FreeSurface(surface[5].surface);
			SDL_BlitSurface(surface[7].surface, NULL, SDL_GetWindowSurface(cubulus->fenetre), &surface[7].position);
			surface[4].surface = TTF_RenderUTF8_Blended(policePetite, "Adresse IP du serveur :", noir);
			surface[5].surface = TTF_RenderUTF8_Blended(policePetite, cubulus->reseau.adresse, noir);
		}

		if(clicHeberger)
		{
			continuer = false;
			menu = false;
		}

		SDL_UpdateWindowSurface(cubulus->fenetre);
        SDL_Delay(10);
	}

	Joueur j1,j2,j3;

	if(clicRejoindre || clicHeberger)
	{
        cubulus->enReseau = true;
		j3.couleur = VOID;
		j1.joueur = true;
		j2.joueur = true;
		j1.aTonTour = true;
		j2.aTonTour = false;

	}

	if(clicHeberger)
	{
        j1.joueurReseau = false;
        j2.joueurReseau = true;
		cubulus->reseau.stopThread = false;
        cubulus->reseau.thread = SDL_CreateThread((SDL_ThreadFunction)Serveur, NULL, (void*)cubulus);
    }

    if(clicRejoindre)
	{
        j1.joueurReseau = true;
        j2.joueurReseau = false;
		cubulus->reseau.stopThread = false;
        cubulus->reseau.thread = SDL_CreateThread((SDL_ThreadFunction)Client, NULL, (void*)cubulus);
    }

    if(clicRejoindre || clicHeberger)
	{
		menu = false;
		for(int i = 0 ; !cubulus->reseau.connecte ; i++)
		{

			SDL_Delay(10);
			if(clicHeberger)
                if(Hebergement(cubulus))
                {
                    cubulus->reseau.stopThread = true;
                    menu = true;
                    break;
                }
			if (cubulus->reseau.erreur != 0)
			{
				printf("Adresse IP invalide, ou serveur introuvable\n");
				cubulus->reseau.stopThread = true;
				menu = true;
				break;
			}

		}
	}

	for(i=0;i<8;i++)
		SDL_FreeSurface(surface[i].surface);

	TTF_CloseFont(policeGros);
    TTF_CloseFont(police);
    TTF_CloseFont(policePetite);

    if(!menu)
    {
        ParametrerCube(&cubulus->cubulus, j1, j2, j3);

        enum Couleur couleurJ1 = Tirage(cubulus);
        cubulus->cubulus.joueurs[2].aTonTour = false;
        if(couleurJ1 == NOIR)
        {
            cubulus->cubulus.joueurs[0].aTonTour = true;
            cubulus->cubulus.joueurs[1].aTonTour = false;
        }

        if(couleurJ1 == ROUGE)
        {
            cubulus->cubulus.joueurs[0].aTonTour = false;
            cubulus->cubulus.joueurs[1].aTonTour = true;
        }
    }

    return menu;

}

