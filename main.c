#include "MoteurGraphique.h"
#include "MoteurJeu.h"

/**
 * \file main.c
 * \brief Comme son nom l'indique, c'est le fichier qui réunit les fonctions
 * 		  des différents modules afin de mettre en place le jeu.
 */



int main(int argc, char *argv[])
{
    CubulusContext *cubulus = malloc(sizeof(CubulusContext));

    if(Initialisation(cubulus) == -1)
        return -1;
    if(!InitialisationReseau(&cubulus->reseau))
        return -1;

    bool menu = true;
    bool continuer = true;

    while(menu)
    {
        switch(Menu(cubulus))
        {
            case 0: // Jouer
                continuer = true;
                break;
            case 1: //Quitter
                continuer = false;
                menu = false;
                break;
        }

        while (continuer)
        {
            switch(GestionEvenements(cubulus))
            {
                case 1:
                    continuer = false;
                    cubulus->opencv.stopThread = true;
                    break;
            }
            Draw(cubulus);
            LimiteFps(7);
        }
    }

    cubulus->reseau.stopThread = true;
    cubulus->opencv.stopThread = true;

    DestroyReseau(&cubulus->reseau);
    DestroyCubulus(cubulus);
    free(cubulus);
	return 0;
}


